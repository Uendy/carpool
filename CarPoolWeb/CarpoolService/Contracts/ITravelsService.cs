﻿using CarpoolData.DTOAndMapper;
using CarpoolData.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
    public interface ITravelsService
    {
        Task<TravelDTO> CreateTravelAsync(DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId);
        Task<IEnumerable<TravelDTO>> GetAllAsync();
        Task<TravelDTO> GetByIdAsync(int travelId);
        Task<IEnumerable<TravelDTO>> GetByDepartureTimeAsync(DateTime departureTime);
        Task<IEnumerable<TravelDTO>> GetByStartLocationAsync(int startLocationId);
        Task<IEnumerable<TravelDTO>> GetByDestinationAsync(int destinationLocationId);
        Task<IEnumerable<TravelDTO>> GetByAvailableSeatsAsync();
        Task<IEnumerable<TravelDTO>> GetByStatusAsync(Status status);
        //Task<IEnumerable<TravelDTO>> MultipleQueryFilterAsync();
        Task<TravelDTO> UpdateAsync(int travelId, DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId);
        Task<TravelDTO> DeleteOwnTravelAsync(int travelId);
        Task<TravelDTO> AddUserToTravelAsync(int ownerId, int travelId, int userId);

        //Task<TravelDTO> AddStopToTravelAsync();
    }
}
