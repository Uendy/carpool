﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class CityService : ICityService
    {
        private readonly CarpoolDBContect Db;
        public CityService(CarpoolDBContect db)
        {
            this.Db = db;
        }
        public async Task<IEnumerable<CityDTO>> GetCitiesAsync()
        {
            var cities = await this.Db.Cities
                .Include(c => c.Country)
                .Include(r => r.Addresses)
                .Select(u => new CityDTO(u)).ToListAsync();

            if (cities == null || cities.Count() == 0)
            { 
                throw new EntityNotFoundException("No Cities exist");
            }

            return cities;
        }

        public async Task<CityDTO> GetCityByIdAsync(int cityId)
        {
            var city = await this.Db.Cities
             .Include(t => t.Addresses)
             .Include(x => x.Country)
             .FirstOrDefaultAsync(x => x.Id == cityId);

            if (city == null)
            {
                throw new EntityNotFoundException($"City with Id: {cityId} does not exist");
            }

            var response = new CityDTO(city);
            return response;
        }
    }
}
