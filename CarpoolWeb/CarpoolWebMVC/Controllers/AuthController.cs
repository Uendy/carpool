﻿
using CarpoolData.DTOAndMapper;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using CarpoolWebMVC.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserService userService;
        private readonly ModelMapper modelMapper;
        private readonly IMailClient mail;
        public AuthController(IUserService userService, ModelMapper modelMapper, IMailClient mail)
        {
            this.userService = userService;
            this.modelMapper = modelMapper;
            this.mail = mail;

        }
        public IActionResult Login()
        {
            return View();
        }
        //public IActionResult Register()
        //{
        //    return View();
        //}

        private async Task<UsersDTO> TryGetUser(string username, string password)
        {
            var user = await this.userService.GetByUsernameAsync(username);

            if (user.Password != password)
            {
                throw new AuthenticationException("Invalid password");
            }
            return user;
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }
            try
            {
                var user = await this.TryGetUser(loginViewModel.Username, loginViewModel.Password);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Role, user.Role)
                };
                var claimsIdentity = new ClaimsIdentity(claims, "Login");

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToAction("Index", "Home");
            }
            catch (EntityNotFoundException ex)
            {

                this.ModelState.AddModelError(nameof(loginViewModel.Username), ex.Message);
                return this.View(loginViewModel);
            }

        }

        //[HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Register()
        {
            var registerViewModel = new RegisterViewModel();

            return this.View(registerViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromForm] RegisterViewModel registerViewModel)
        {
            try
            {

                if (!this.ModelState.IsValid)
                {
                    return this.View(registerViewModel);
                }

                var user = this.modelMapper.ToModel(registerViewModel);

                await this.userService.CreateAsync(user.UserName, user.Password, user.FirstName, user.LastName, user.Email, user.PhoneNumber, user.ValidationCode, registerViewModel.Avatar);
                await mail.SendValidationEmail(user);
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }
        public async Task<IActionResult> Error404()
        {
            return this.View();
        }


    }
}