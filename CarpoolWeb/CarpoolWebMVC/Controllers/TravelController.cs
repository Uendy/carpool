﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using CarpoolService.Contracts;
using CarpoolData.Models;
using Microsoft.AspNetCore.Authorization;

namespace CarpoolWebMVC.Controllers
{
    public class TravelController : Controller
    {
        private readonly ITravelsService travelService;
        public TravelController(ITravelsService travel)
        {
            this.travelService = travel;
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetTravelsAsync()
        { // https://...../Travel/GetTravels
            var travels = await this.travelService.GetAllAsync();

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetTravelByIdAsync(int id)
        {
            var travel = await this.travelService.GetByIdAsync(id);

            if (travel == null)
            {
                return NotFound();
            }

            return this.View(travel);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> UpdateTravelAsync(int travelId, DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId)
        {
            var travelToUpdate = await this.travelService.UpdateAsync(travelId, departureTime, arrivalTime, availableSeats, breaks, startLocationId, destinationLocationId, carId, statusId);

            if (travelToUpdate == null)
            {
                return NotFound();
            }

            return this.View(travelToUpdate);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> CreateTravelAsync(DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId)
        {
            var travel = await this.travelService.CreateTravelAsync(departureTime, arrivalTime, availableSeats, breaks, startLocationId, destinationLocationId, carId, statusId);

            if (travel == null)
            {
                return NotFound();
            }

            return this.View(travel);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteTravelAsync(int id)
        {
            var travelToDelete = await this.travelService.DeleteOwnTravelAsync(id);

            return this.View(); // should this return something?
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetByDepartureTimeAsync(DateTime dateTime)
        {
            var travels = await this.travelService.GetByDepartureTimeAsync(dateTime);

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetTravelByDestinationAsync(int destinationLocationId)
        {
            var travels = await this.travelService.GetByDestinationAsync(destinationLocationId);

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetByTravelByStartLocationAsync(int startlocationId)
        {
            var travels = await this.travelService.GetByStartLocationAsync(startlocationId);

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetByAvailableSeatsAsync()
        {
            var travels = await this.travelService.GetByAvailableSeatsAsync();

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetByStatusAsync(Status status)
        {
            var travels = await this.travelService.GetByStatusAsync(status);

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> AddUserToTravelAsync(int ownerId, int travelId, int userId)
        {
            var travel = await this.travelService.AddUserToTravelAsync(ownerId, travelId, userId);

            if (travel == null)
            {
                return NotFound();
            }

            return this.View(travel);
        }
    }
}