﻿using CarpoolService.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountriesService countryService;
        public CountryController(ICountriesService countryService)
        {
            this.countryService = countryService;
        }

        public async Task <IActionResult> Index()
        {
            var countries = await this.countryService.GetCountriesAsync();

            if (!countries.Any())
            {
                return NotFound();
            }

            return this.View(countries);
        }
        public async Task<IActionResult> Details(int id)
        {
            var city = await this.countryService.GetCountriesByIdAsync(id);

            if (city == null)
            {
                return NotFound();
            }

            return this.View(city);
        }
    }
}
