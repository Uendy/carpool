﻿using CarpoolService.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Controllers
{
    public class CarControllers : Controller
    {
        private readonly ICarsService carsService;
        public CarControllers(ICarsService cars)
        {
            carsService = cars;
        }
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var cars = await this.carsService.GetAllAsync();

            if (!cars.Any())
            {
                return NotFound();
            }

            return this.View(cars);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> Details([FromRoute] int id)
        {
            var cars = await this.carsService.GetByIdAsync(id);

            if (cars == null)
            {
                return NotFound();
            }

            return this.View(cars);
        }
        [Authorize(Roles = "User")]
        public async Task<IActionResult> UpdateStatus([BindRequired] string registration, [BindRequired] int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed)
        {
            var cars = await this.carsService.UpdateAsync(registration, userId, totalSeats, brand, model, color, canSmoke, petsAllowed);
            if (cars == null)
            {
                return NotFound();
            }
            return this.View(cars);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> CreateCarAsync([BindRequired] string registration, [BindRequired] int userId, [BindRequired] int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed)
        {
            var cars = await this.carsService.CreateCarAsync(registration, userId, totalSeats, brand, model, color, canSmoke, petsAllowed);
            if (cars==null)
            {
                return NotFound();
            }
            return this.View(cars);

        }
        [Authorize(Roles = "User")]
        public async Task<IActionResult> DeleteOwnCarAsync([BindRequired] int userId, [BindRequired] string registration)
        {
            await this.carsService.DeteleOwnCarAsync(userId, registration);

            return this.View();

        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> FilterMultipleCriteriaAsync([FromQuery] bool petsAllowed, bool canSmoke)
        {
            var cars = await this.carsService.MultipleQueryFilterAsync(petsAllowed, canSmoke);
            if (cars== null)
            {
                return NotFound();
            }
            return this.View(cars);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetByOwnerAsync([FromRoute] int id)
        {
            var cars = await this.carsService.GetByOwnerAsync(id);
            if (cars==null)
            {
                return NotFound();
            }
            return this.View(cars);

        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> FilterBySmokeAsync([FromQuery] bool smoke)
        {
            var cars = await this.carsService.FilterBySmokeAsync(smoke);

            if (cars == null)
            {
                return NotFound();
            }
            return this.View(cars);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> FilterByPetsAsync([FromQuery] bool pets)
        {
            var cars = await this.carsService.FilterByPetsAllowerdAsync(pets);
            if (cars == null)
            {
                return NotFound();
            }
            return this.View(cars);
        }
    }
}
