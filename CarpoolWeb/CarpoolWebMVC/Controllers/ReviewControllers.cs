﻿using CarpoolService.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Controllers
{
    public class ReviewControllers : Controller
    {
        private readonly IReviewService reviewService;
        public ReviewControllers(IReviewService review)
        {
            this.reviewService = review;
        }

        public async Task<IActionResult> Index()
        {
            var reviews = await this.reviewService.GetAllAsync();

            if (!reviews.Any())
            {
                return NotFound();
            }

            return this.View(reviews);
        }
        public async Task<IActionResult> Details([FromRoute] int id)
        {
            var reviews = await this.reviewService.GetByIdAsync(id);

            if (reviews == null)
            {
                return NotFound();
            }

            return this.View(reviews);
        }
        public async Task<IActionResult> UpdateStatus([BindRequired] int id, string comment, decimal rating)
        {
            var reviews = await this.reviewService.UpdateAsync(id, comment, rating);
            if (reviews == null)
            {
                return NotFound();               
            }
            return this.View(reviews);
        }
    }
}
