﻿using CarpoolService.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Controllers
{
    public class CityController : Controller
    {
        private readonly ICityService cityService;
        public CityController(ICityService cityService)
        {
            this.cityService = cityService;
        }
        public async Task <IActionResult> Index()
        {
            var cities = await this.cityService.GetCitiesAsync();

            if (!cities.Any())
            {
                return NotFound();
            }

            return this.View(cities);
        }
        public async Task <IActionResult> Details(int id)
        {
            var city = await this.cityService.GetCityByIdAsync(id);

            if (city == null)
            {
                return NotFound();
            }

            return this.View(city);
        }
    }
}
