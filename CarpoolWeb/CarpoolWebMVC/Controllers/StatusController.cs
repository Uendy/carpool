﻿using CarpoolService.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Controllers
{
    public class StatusController : Controller
    {
        private readonly IStatusService statusService;
        public StatusController(IStatusService status)
        {
            this.statusService = status;
        }
        public async Task <IActionResult> Index()
        {
            var status = await this.statusService.GetAllAsync();

            if (!status.Any())
            {
                return NotFound();
            }

            return this.View(status);
        }
        public async Task<IActionResult> Details(int id)
        {
            var status = await this.statusService.GetByIdAsync(id);

            if (status == null)
            {
                return NotFound();
            }

            return this.View(status);
        }
        public async Task<IActionResult> UpdateStatus([BindRequired] int id, [BindRequired] string statusType)
        {
            var statues = await this.statusService.UpdateAsync(id, statusType);
            if (statues == null)
            {
                return NotFound();
            }
            return this.View(statues);
        }
    }
}
