﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using CarpoolService.Contracts;
using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace CarpoolWebMVC.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private static Random random = new Random();

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [Authorize(Roles = "User, Anonymous")]
        public async Task<IActionResult> GetUsersAsync()
        {
            var users = await this.userService.GetAll();

            if (!users.Any())
            {
                return NotFound();
            }

            return this.View(users);
        }
        public async Task<IActionResult> GetUserByIdAsync(int id)
        {
            var user = await this.userService.GetByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return this.View(user);
        }

        public async Task<IActionResult> UpdateUserAsync(string username)
        {
            var user = await this.userService.GetByUsernameAsync(username);

            return this.View(user);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserAsync(int userId, string username, string password, string firstName, string lastName, string email, string phoneNumber)
        {
            var user = await this.userService.UpdateAsync(userId, username, password, firstName, lastName, email, phoneNumber);

            if (user == null)
            {
                return NotFound();
            }

            return this.View(user);
        }

        public async Task<IActionResult> CreateUserAsync(string userName, string password, string firstName, string lastName, string email, string phoneNumber, IFormFile avatar)
        {
            try
            {
                var validationString = RandomString(5);
                var user = await this.userService.CreateAsync(userName, password, firstName, lastName, email, phoneNumber, validationString, avatar); ;

                if (user == null)
                {
                    return NotFound(); // should be something like: invalid input
                }

                return this.View(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<IActionResult> GetByUsernameAsync(string username)
        {
            var user = await this.userService.GetByUsernameAsync(username);

            if (user == null)
            {
                return NotFound();
            }

            return this.View(user);
        }

        public async Task<IActionResult> SearchByEmaileAsync(string email)
        {
            var users = await this.userService.SearchByEmailAsync(email);

            if (!users.Any())
            {
                return NotFound();
            }

            return this.View(users);
        }
        public async Task<IActionResult> GetByPhoneAsync(string phone)
        {
            var user = await this.userService.GetByPhoneAsync(phone);

            if (user == null)
            {
                return NotFound();
            }

            return this.View(user);
        }
        public async Task<IActionResult> FilterByRatingAsync(decimal rating)
        {
            var users = await this.userService.FilterByRatingAsync(rating);

            if (!users.Any())
            {
                return NotFound();
            }

            return this.View(users);
        }
        public async Task<IActionResult> FilterMultipleCriteriaAsync(string firstname, string lastname, string email, string phone, string username)
        {
            var users = await this.userService.MultipleQueryFilterAsync(firstname, lastname, email, phone, username);

            if (!users.Any())
            {
                return NotFound();
            }

            return this.View(users);
        }

        public async Task<IActionResult> DeleteOwnUserAsync(int id)
        {
            await this.userService.DeleteOwnUserAsync(id);

            return this.View(); // Should this return something?
        }

        public async Task<IActionResult> DeleteReviewAsync(int userId, int reviewId)
        {
            await this.userService.DeleteReviewAsync(userId, reviewId);

            return this.View(); // Should this return something?
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetUserCarsAsync(int id)
        {
            var cars = await this.userService.SeeAllHisCarsAsync(id);

            if (!cars.Any())
            {
                return NotFound();
            }

            return this.View(cars);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetUserRecipientReviewsAsync(int id)
        {
            var reviews = await this.userService.SeeAllHisRecipientReviewsAsync(id);

            if (!reviews.Any())
            {
                return NotFound();
            }

            return this.View(reviews);
        }
        [Authorize(Roles = "User,Admin")]
        public async Task<IActionResult> GetUserAuthorReviewsAsync(int id)
        {
            var reviews = await this.userService.SeeAllHisAuthorReviewsAsync(id);

            if (!reviews.Any())
            {
                return NotFound();
            }

            return this.View(reviews);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> GetAverageRatingAsync(int id)
        {
            var rating = await this.userService.AverageRatingAsync(id);

            if (rating == default(decimal))
            {
                return NotFound();
            }

            return this.View(rating);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> CreateReviewAsync(int userId, int recipientId, string comment, decimal rating)
        {
            var review = await this.userService.LeaveReviewAsync(userId, recipientId, comment, rating);

            if (review == null)
            {
                return NotFound();
            }

            return this.View(review);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> EditReviewAsync(int userId, int reviewId, string comment, decimal rating)
        {
            var review = await this.userService.EditReviewAsync(userId, reviewId, comment, rating);

            if (review == null)
            {
                return NotFound();
            }

            return this.View(review);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> OwnerTravelHistoryAsync(int id)
        {
            var travels = await this.userService.OwnTravelHistory(id);

            if (!travels.Any())
            {
                return NotFound();
            }

            return this.View(travels);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> GetAllRecipientRequest(int recipientId)
        {
            var requests = await this.userService.SeeAllHisRecipientRequestAsync(recipientId);

            if (!requests.Any())
            {
                return NotFound();
            }

            return this.View(requests);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> GetAllAuthorRequest(int authorId)
        {
            var requests = await this.userService.SeeAllHisAuthorRequestAsync(authorId);

            if (!requests.Any())
            {
                return NotFound();
            }

            return this.View(requests);
        }
        [Authorize(Roles = "User,Admin")]

        public async Task<IActionResult> CreateTravelRequest(int authorId, int recipientId, int travelId)
        {
            var request = await this.userService.CreateRequestAsync(authorId, recipientId, travelId);

            if (request == null)
            {
                return NotFound();
            }

            return this.View(request);
        }

        public async Task<IActionResult> RespondToTravelRequest(int recipientId, int requestId, bool answer)
        {
            var request = await this.userService.EditRequestAsync(recipientId, requestId, answer);

            if (request == null)
            {
                return NotFound();
            }

            return this.View(request);
        }

        public async Task<IActionResult> DeleteTravelRequest(int authorId, int requestId)
        {
            await this.userService.DeleteRequestAsync(authorId, requestId);

            return this.View(); // Should this return something?
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}