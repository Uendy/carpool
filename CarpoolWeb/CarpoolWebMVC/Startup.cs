using CarpoolData.CarpoolDBContext;
using CarpoolService.Contracts;
using CarpoolService.ModelService;
using CarpoolWebMVC.Models;
using CloudinaryDotNet;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace CarpoolWebMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CarpoolDBContect>(options =>
                      options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDistributedMemoryCache();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(o =>
                    {
                        o.LoginPath = "/Auth/Login";
                        o.AccessDeniedPath = "/Auth/Error404";
                        o.Cookie.Name = "auth_cookie";
                        o.SlidingExpiration = true;
                        o.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                    });

            services.AddControllersWithViews();
            services.AddScoped<ModelMapper>();
            services.AddScoped<IMailClient, MailClient>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<ICountriesService, CountriesService>();
            services.AddScoped<ICarsService, CarsService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IStatusService, StatusService>();
            services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IAdminsService, AdminsService>();
            services.AddScoped<ITravelsService, TravelService>();
            services.AddScoped<ICloudinaryService, CloudinaryService>();
            var cloudinary = new Cloudinary(new Account("dnnibpx64", "955872244291882", "JZ5B1JnCLt059vch_CUujbBaycI"));
            services.AddSingleton(cloudinary);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
           
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
