﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class CityViewModel: EntityViewModel
    {

        public string CityName { get; set; }
    }
}
