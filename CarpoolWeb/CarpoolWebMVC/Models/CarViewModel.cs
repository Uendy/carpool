﻿using CarpoolData.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class CarViewModel: EntityViewModel
    {
        [Required, MinLength(6), MaxLength(10)]
        public string Registration { get; set; }
        [Required]
        public User Owner { get; set; }
        [Required]
        public int TotalSeats { get; set; }
        [Required]
        public string Brand { get; set; }
        public string Model { get; set; }
        [Required]
        public string Color { get; set; }
        public bool CanSmoke { get; set; }
        public bool PetsAllowed { get; set; }
        public int UserId { get; set; }
    }
}
