﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class RegisterViewModel: LoginViewModel
    {
        [Required]
        public string ConfirmPassword { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string FirstName { get; set; }

       
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string LastName { get; set; }
        [StringLength(10, ErrorMessage = "Phone number is invalid!")]
       [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Phone number is invalid!")]
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        [Required]
        [MaxLength(100, ErrorMessage = "Email length must be {1}!"), EmailAddress(ErrorMessage = "Invalid value for {0}")]
        public string Email { get; set; }
        public IFormFile Avatar { get; set; }
    }
}
