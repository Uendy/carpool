﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class TravelViewMode:EntityViewModel
    {
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public int AvailableSeat { get; set; }
        public bool Breaks { get; set; }
        public string StartLocation { get; set; }
        public string Destination { get; set; }
        public string Car { get; set; }
        public string Status { get; set; }
    }
}
