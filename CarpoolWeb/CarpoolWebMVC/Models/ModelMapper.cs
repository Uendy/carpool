﻿using CarpoolData.Models;

namespace CarpoolWebMVC.Models
{
    public class ModelMapper
    {
        public User UserViewModel(UserViewModel userWebModel)
        {
            return new User
            {
                FirstName = userWebModel.FirstName,
                LastName = userWebModel.LastName,
                Email = userWebModel.Email,
                PhoneNumber = userWebModel.PhoneNumber,
                UserName = userWebModel.Username,
                RoleId = userWebModel.RoleId,
                ProfileImage = userWebModel.ProfileImage,
            };
        }
        public User ToModel(RegisterViewModel registerViewModel)
        {
            return new User
            {
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                UserName = registerViewModel.Username,
                Password = registerViewModel.Password,
                PhoneNumber = registerViewModel.PhoneNumber,              
                Email = registerViewModel.Email

            };
        }
        public Car CarViewModel(CarViewModel carViewModel)
        {
            return new Car
            {
                Registration = carViewModel.Registration,
                Owner = carViewModel.Owner,
                Brand = carViewModel.Brand,
                Model = carViewModel.Model,
                Color = carViewModel.Color,
                CanSmoke = carViewModel.CanSmoke,
                PetsAllowed = carViewModel.PetsAllowed,
            };
        }
        public City CityViewModel(CityViewModel cityWebModel)
        {
            return new City
            {
                Name = cityWebModel.CityName
            };
        }
        public Country CountryViewModel(CountryViewModel countryWebModel)
        {
            return new Country
            {
                Name = countryWebModel.CountryName
            };
        }
        public Address ToModel(AddressViewModel addressWebModel)
        {
            return new Address
            {
                Name = addressWebModel.Name,
                CityId = addressWebModel.CityId
            };
        }
        public Review ReviewViewModel(ReviewViewModel reviewViewModel)
        {
            return new Review
            {
                Rating = reviewViewModel.Rating,
                Comment = reviewViewModel.Comment,
            };
        }
    }
}

