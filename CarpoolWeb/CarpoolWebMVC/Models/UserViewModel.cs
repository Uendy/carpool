﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class UserViewModel: EntityViewModel
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id must be a positive number.")]
        public int UserId { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string FirstName { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string LastName { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        public int RoleId { get; set; }

        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public int TravelId { get; set; }
        public decimal AverageRating { get; set; }
        public string ProfileImage { get; set; }


    }
}
