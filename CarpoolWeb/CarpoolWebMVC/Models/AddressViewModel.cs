﻿
namespace CarpoolWebMVC.Models
{
    public class AddressViewModel:EntityViewModel
    {
        public string Name { get; set; }
        public int CityId { get; set; }
    }
}
