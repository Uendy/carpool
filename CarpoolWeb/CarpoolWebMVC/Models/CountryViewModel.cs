﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class CountryViewModel: EntityViewModel
    {
        public string CountryName { get; set; }
    }
}
