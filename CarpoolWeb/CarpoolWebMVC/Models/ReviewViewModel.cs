﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolWebMVC.Models
{
    public class ReviewViewModel: EntityViewModel
    {
        
        public string Author { get; set; }
        public string Recipient { get; set; }
        public decimal Rating { get; set; }
        public string Comment { get; set; }
    }
}
