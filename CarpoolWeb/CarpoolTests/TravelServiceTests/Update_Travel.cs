﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.TravelServiceTests
{
    [TestClass]
    public class Update_Travel
    {
        private ICarsService carsService;
        private CarpoolDBContect fakeDb;
        private IUserService userService;
        private ITravelsService travelService;

        [TestMethod]
        public async Task Should_Throw_Exception_Update_Travel()
        {
            var expectedMessage = "Travel with Id: 0 does not exist.";

            var addresses = new List<Address>()
            {
                new Address
                {
                    Id = 1,
                    Name = "Sofia"
                },
                new Address
                {
                    Id = 2,
                    Name = "Lovech"
                }
            };

            var statuses = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    Registration = "OB8888CA",
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    TotalSeats = 5,
                    CanSmoke = true,
                    PetsAllowed = false,
                    User = new User
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        UserName = "username",
                    }
                },
                new Car
                {
                    Id = 2,
                    Registration = "CA4444CA",
                    Brand = "Audi",
                    Model = "R8",
                    Color = "Red",
                    TotalSeats = 5,
                    CanSmoke = false,
                    PetsAllowed = true,
                    User = new User
                    {
                        Id = 2,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        UserName = "username1",
                    }
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    UserName = "username",
                },
                new User
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    UserName = "username1",
                }
            };

            var travels = new List<Travel>()
            {
                new Travel
                {
                    Id = 1,
                    StartLocationId = 1,
                    StartLocation = addresses[0],
                    DestinationId = 2,
                    Destination = addresses[1],
                    DepartureTime = DateTime.Parse("21-11-2021"),
                    ArrivalTime = DateTime.Parse("22-11-2021"),
                    CarId = 1,
                    Car = cars[0],
                    AvailableSeat = 3,
                    StatusId = 1,
                    Status = statuses[0],
                    Breaks = false
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var addressMock = addresses.AsQueryable().BuildMockDbSet();
            var statusMock = statuses.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressMock.Object);
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new TravelService(dbMock.Object, this.carsService);
            var actualMessage = "";
            int id = 0;

            var departureTime = DateTime.Parse("22-11-2021");
            var arrivalTime = DateTime.Parse("23-11-2021");
            var availableSeats = 3;
            var breaks = false;
            var startLocationId = 1;
            var destinationLocationId = 2;
            var carId = 1;
            var statusId = 1;


            try
            {
                await service.UpdateAsync(id, departureTime, arrivalTime, availableSeats, breaks, startLocationId, destinationLocationId, carId, statusId);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Update_Travel_Test()
        {
            var addresses = new List<Address>()
            {
                new Address
                {
                    Id = 1,
                    Name = "Sofia"
                },
                new Address
                {
                    Id = 2,
                    Name = "Lovech"
                }
            };

            var statuses = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    Registration = "OB8888CA",
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    TotalSeats = 5,
                    CanSmoke = true,
                    PetsAllowed = false,
                    User = new User
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        UserName = "username",
                    }
                },
                new Car
                {
                    Id = 2,
                    Registration = "CA4444CA",
                    Brand = "Audi",
                    Model = "R8",
                    Color = "Red",
                    TotalSeats = 5,
                    CanSmoke = false,
                    PetsAllowed = true,
                    User = new User
                    {
                        Id = 2,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        UserName = "username1",
                    }
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    UserName = "username",
                },
                new User
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    UserName = "username1",
                }
            };

            var travels = new List<Travel>()
            {
                new Travel
                {
                    Id = 1,
                    StartLocationId = 1,
                    StartLocation = addresses[0],
                    DestinationId = 2,
                    Destination = addresses[1],
                    DepartureTime = DateTime.Parse("21-11-2021"),
                    ArrivalTime = DateTime.Parse("22-11-2021"),
                    CarId = 1,
                    Car = cars[0],
                    AvailableSeat = 3,
                    StatusId = 1,
                    Status = statuses[0],
                    Breaks = false
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var addressMock = addresses.AsQueryable().BuildMockDbSet();
            var statusMock = statuses.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressMock.Object);
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new TravelService(dbMock.Object, this.carsService);
            int id = 1;

            var departureTime = DateTime.Parse("22-11-2021");
            var arrivalTime = DateTime.Parse("23-11-2021");
            var availableSeats = 3;
            var breaks = false;
            var startLocationId = 1;
            var destinationLocationId = 2;
            var carId = 1;
            var statusId = 1;

            await service.UpdateAsync(id, departureTime, arrivalTime, availableSeats, breaks, startLocationId, destinationLocationId, carId, statusId);

            Assert.AreEqual(travelMock.Object.First().DepartureTime, departureTime);
            Assert.AreEqual(travelMock.Object.First().ArrivalTime, arrivalTime);
            Assert.AreEqual(travelMock.Object.First().AvailableSeat, availableSeats);
            Assert.AreEqual(travelMock.Object.First().Breaks, breaks);
            Assert.AreEqual(travelMock.Object.First().StartLocationId, startLocationId);
            Assert.AreEqual(travelMock.Object.First().DestinationId, destinationLocationId);
            Assert.AreEqual(travelMock.Object.First().StatusId, statusId);
        }
    }
}