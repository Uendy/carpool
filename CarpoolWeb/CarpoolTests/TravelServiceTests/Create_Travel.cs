﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.TravelServiceTests
{
    [TestClass]
    public class Create_Travel
    {
        private ICarsService carsService;
        private CarpoolDBContect fakeDb;
        private IUserService userService;
        private ITravelsService travelService;

        // Create All Travel Tests: 

        [TestInitialize]
        public void SetUp()
        {
            var addresses = new List<Address>()
            {
                new Address
                {
                    Id = 1,
                    Name = "Sofia"
                },
                new Address
                {
                    Id = 2,
                    Name = "Lovech"
                }
            };

            var statuses = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    Registration = "OB8888CA",
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    TotalSeats = 5,
                    CanSmoke = true,
                    PetsAllowed = false,
                    User = new User
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        UserName = "username",
                    }
                },
                new Car
                {
                    Id = 2,
                    Registration = "CA4444CA",
                    Brand = "Audi",
                    Model = "R8",
                    Color = "Red",
                    TotalSeats = 5,
                    CanSmoke = false,
                    PetsAllowed = true,
                    User = new User
                    {
                        Id = 2,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        UserName = "username1",
                    }
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    UserName = "username",
                },
                new User
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    UserName = "username1",
                }
            };

            var travels = new List<Travel>()
            {
                new Travel
                {
                    Id = 1,
                    StartLocationId = 1,
                    StartLocation = addresses[0],
                    DestinationId = 2,
                    Destination = addresses[1],
                    DepartureTime = DateTime.Parse("21-11-2021"),
                    ArrivalTime = DateTime.Parse("22-11-2021"),
                    CarId = 1,
                    Car = cars[0],
                    AvailableSeat = 3,
                    StatusId = 1,
                    Status = statuses[0],
                    Breaks = false
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var addressMock = addresses.AsQueryable().BuildMockDbSet();
            var statusMock = statuses.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressMock.Object);
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;
        }

        [TestMethod]
        public async Task Create_Task_Test()
        {
            var cars = fakeDb.Cars;

            var registration = "OB6666CA";
            int userId = 1;
            int totalSeats = 5;
            string brand = "Peaugot";
            string model = "3008";
            string color = "Blue";
            bool canSmoke = false;
            bool petsAllowed = false;

            var service = new CarsService(this.fakeDb, this.userService);
            var result = await service.CreateCarAsync(registration, userId, totalSeats, brand, model, color, canSmoke, petsAllowed);
            //fakeDb.Travels.Add(new Travel);

            Assert.AreEqual(2, cars.Count());
        }
    }
}