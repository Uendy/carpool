﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.TravelServiceTests
{
    [TestClass]
    public class Get_Travel
    {
        private ICarsService carsService;
        private CarpoolDBContect fakeDb;
        private IUserService userService;
        private ITravelsService travelService;

        // Get All Travel Tests: 

        [TestInitialize]
        public void SetUp()
        {
            var addresses = new List<Address>()
            {
                new Address
                {
                    Id = 1,
                    Name = "Sofia"
                },
                new Address
                {
                    Id = 2,
                    Name = "Lovech"
                }
            };

            var statuses = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    Registration = "OB8888CA",
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    TotalSeats = 5,
                    CanSmoke = true,
                    PetsAllowed = false,
                    User = new User
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        UserName = "username",
                    }
                },
                new Car
                {
                    Id = 2,
                    Registration = "CA4444CA",
                    Brand = "Audi",
                    Model = "R8",
                    Color = "Red",
                    TotalSeats = 5,
                    CanSmoke = false,
                    PetsAllowed = true,
                    User = new User
                    {
                        Id = 2,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        UserName = "username1",
                    }
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    UserName = "username",
                },
                new User
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    UserName = "username1",
                }
            };

            var travels = new List<Travel>()
            {
                new Travel
                {
                    Id = 1,
                    StartLocationId = 1,
                    StartLocation = addresses[0],
                    DestinationId = 2,
                    Destination = addresses[1],
                    DepartureTime = DateTime.Parse("21-11-2021"),
                    ArrivalTime = DateTime.Parse("22-11-2021"),
                    CarId = 1,
                    Car = cars[0],
                    AvailableSeat = 3,
                    StatusId = 1,
                    Status = statuses[0],
                    Breaks = false
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var addressMock = addresses.AsQueryable().BuildMockDbSet();
            var statusMock = statuses.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressMock.Object);
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;
        }

        [TestMethod]
        public async Task Should_Throw_No_Travles_Found_Exception()
        {
            var expectedMessage = "There are currently no travels.";

            var travelsMockList = new List<Travel>();

            var travelMock = travelsMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);

            var service = new TravelService(dbMock.Object, carsService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetAllAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travels()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            var result = await service.GetAllAsync();

            Assert.AreEqual(expectedStartLocation, result.First().StartLocation);
            Assert.AreEqual(expectedDestination, result.First().Destination);
            Assert.AreEqual(expectedDepartureTime, result.First().DepartureTime);
            Assert.AreEqual(expectedCarId, result.First().Car);
            Assert.AreEqual(expectedStatus, result.First().Status);
            Assert.AreEqual(expectedBreaks, result.First().Breaks);
        }

        // Get Travel By Id Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Invalid_Id()
        {
            var expectedMessage = "Travel with Id: 10 does not exist.";

            var service = new TravelService(this.fakeDb, this.carsService);
            int id = 10;

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travel_By_Id()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            int id = 1;
            var result = await service.GetByIdAsync(id);

            Assert.AreEqual(expectedStartLocation, result.StartLocation);
            Assert.AreEqual(expectedDestination, result.Destination);
            Assert.AreEqual(expectedDepartureTime, result.DepartureTime);
            Assert.AreEqual(expectedCarId, result.Car);
            Assert.AreEqual(expectedStatus, result.Status);
            Assert.AreEqual(expectedBreaks, result.Breaks);
        }

        // Get By DepartureTime Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_No_Travel_With_DepartureTime()
        {
            var departureTime = DateTime.Parse("23-11-2021");
            var expectedMessage = $"Travels with Departure Time: {departureTime.ToString("yyyy-MM-dd-ss-ff")} do not exist.";

            var service = new TravelService(this.fakeDb, this.carsService);
            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByDepartureTimeAsync(departureTime);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travel_By_Departure_Time()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            var departureTime = DateTime.Parse("21-11-2021");
            var result = await service.GetByDepartureTimeAsync(departureTime);

            Assert.AreEqual(expectedStartLocation, result.First().StartLocation);
            Assert.AreEqual(expectedDestination, result.First().Destination);
            Assert.AreEqual(expectedDepartureTime, result.First().DepartureTime);
            Assert.AreEqual(expectedCarId, result.First().Car);
            Assert.AreEqual(expectedStatus, result.First().Status);
            Assert.AreEqual(expectedBreaks, result.First().Breaks);
        }

        // Get By Destination Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_No_Travel_With_Destinaion()
        {
            var expectedMessage = "Travels with Destination Location Id : 10 do not exist.";

            var service = new TravelService(this.fakeDb, this.carsService);
            int id = 10;

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByDestinationAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travel_By_Destination()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            var destinationId = 2;
            var result = await service.GetByDestinationAsync(destinationId);

            Assert.AreEqual(expectedStartLocation, result.First().StartLocation);
            Assert.AreEqual(expectedDestination, result.First().Destination);
            Assert.AreEqual(expectedDepartureTime, result.First().DepartureTime);
            Assert.AreEqual(expectedCarId, result.First().Car);
            Assert.AreEqual(expectedStatus, result.First().Status);
            Assert.AreEqual(expectedBreaks, result.First().Breaks);
        }

        // Get By Start Location Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_No_Travel_With_StartLocation()
        {
            var expectedMessage = "Travels with Start Location Id : 10 do not exist.";

            var service = new TravelService(this.fakeDb, this.carsService);
            int id = 10;

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByStartLocationAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travel_By_StartLocation()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            var startLocationId = 1;
            var result = await service.GetByStartLocationAsync(startLocationId);

            Assert.AreEqual(expectedStartLocation, result.First().StartLocation);
            Assert.AreEqual(expectedDestination, result.First().Destination);
            Assert.AreEqual(expectedDepartureTime, result.First().DepartureTime);
            Assert.AreEqual(expectedCarId, result.First().Car);
            Assert.AreEqual(expectedStatus, result.First().Status);
            Assert.AreEqual(expectedBreaks, result.First().Breaks);
        }

        // Get By Available Seats:
        [TestMethod]
        public async Task Should_Throw_Exception_No_Travels_With_Available_Seats()
        {
            var expectedMessage = "No Travels with available seats.";

            var travelsMockList = new List<Travel>();

            var travelMock = travelsMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);

            var service = new TravelService(dbMock.Object, carsService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByAvailableSeatsAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travel_With_Available_seats()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            var result = await service.GetByAvailableSeatsAsync();

            Assert.AreEqual(expectedStartLocation, result.First().StartLocation);
            Assert.AreEqual(expectedDestination, result.First().Destination);
            Assert.AreEqual(expectedDepartureTime, result.First().DepartureTime);
            Assert.AreEqual(expectedCarId, result.First().Car);
            Assert.AreEqual(expectedStatus, result.First().Status);
            Assert.AreEqual(expectedBreaks, result.First().Breaks);
        }

        // Get By Status: 
        [TestMethod]
        public async Task Should_Throw_Exception_No_Travel_With_Status()
        {
            var expectedMessage = "No Travels with Status:  available.";

            var service = new TravelService(this.fakeDb, this.carsService);
            var status = new Status();

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByStatusAsync(status);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Travel_By_Status()
        {
            var expectedStartLocation = "Sofia";
            var expectedDestination = "Lovech";
            var expectedDepartureTime = DateTime.Parse("21-11-2021");
            var expectedCarId = "OB8888CA";
            var expectedStatus = "Travelling";
            var expectedBreaks = false;

            var service = new TravelService(this.fakeDb, this.carsService);
            var status = fakeDb.Statuses.First();
            var result = await service.GetByStatusAsync(status);

            Assert.AreEqual(expectedStartLocation, result.First().StartLocation);
            Assert.AreEqual(expectedDestination, result.First().Destination);
            Assert.AreEqual(expectedDepartureTime, result.First().DepartureTime);
            Assert.AreEqual(expectedCarId, result.First().Car);
            Assert.AreEqual(expectedStatus, result.First().Status);
            Assert.AreEqual(expectedBreaks, result.First().Breaks);
        }
    }
}