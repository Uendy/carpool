﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;

namespace CarpoolTests.ReviewServiceTests
{
    [TestClass]
    public class Update_Review
    {
        // Update Methods for Review:
        [TestMethod]
        public async Task Should_ThrowException_When_Id_Does_Not_Exist()
        {
            string expectedMessage = "Review with Id: 2 does not exist";

            var reviewMockList = new List<Review>()
            {
               new Review
               {
                    Id = 1,
                    Comment = "Amazing Driver",
                    Rating = 5,
                    Author = new User()
                    {

                    },
                    Recipient = new User()
                    {

                    }
               }
            };

            var reviewMock = reviewMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);

            var service = new ReviewService(dbMock.Object);
            var actualMessage = string.Empty;
            int id = 2;
            string newComment = "Passable";
            decimal newRating = 4;

            try
            {
                var result = await service.UpdateAsync(id, newComment, newRating);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Update_Reviews()
        {
            string expectedMessage = "Passable";
            decimal expectedRating = 4;

            var reviewMockList = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Comment = "Amazing Driver",
                    Rating = 5,
                    Author = new User()
                    {

                    },
                    Recipient = new User()
                    {

                    }
                }
            };

            var reviewMock = reviewMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);

            var service = new ReviewService(dbMock.Object);
            int id = 1;
            string newComment = "Passable";
            decimal newRating = 4;
            var result = await service.UpdateAsync(id, newComment, newRating);
            var actualMessage = result.Comment;
            var actualRating = result.Rating;

            Assert.AreEqual(expectedMessage, actualMessage);
            Assert.AreEqual(expectedRating, actualRating);
        }
    }
}