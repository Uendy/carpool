﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;

namespace CarpoolTests.ReviewServiceTests
{
    [TestClass]
    public class Get_Review
    {
        // Get All Reviews Service Tests:
        [TestMethod]
        public async Task Get_Reviews_Exception()
        {
            string expectedMessage = "No reviews exist";

            var reviewMockList = new List<Review>();

            var reviewMock = reviewMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);

            var service = new ReviewService(dbMock.Object);
            var actualMessage = string.Empty;
            try
            {
                var result = await service.GetAllAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Reviews()
        {
            string expectedMessage = "Amazing Driver";
            decimal expectedRating = 5;

            var reviewMockList = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Comment = "Amazing Driver",
                    Rating = 5,
                    Author = new User()
                    { 
                        
                    },
                    Recipient = new User()
                    {
                        
                    }
                }
            };

            var reviewMock = reviewMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);

            var service = new ReviewService(dbMock.Object);
            var actualMessage = string.Empty;
            decimal actualRating = 0;
            var result = await service.GetAllAsync();
            foreach (var item in result)
            {
                actualMessage = item.Comment;
                actualRating = item.Rating;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
            Assert.AreEqual(expectedRating, actualRating);
        }

        // Get Review by Id Service Tests:

        [TestMethod]
        public async Task Get_Review_By_Id_Exception()
        {
            string expectedMessage = "Review with Id: 2 does not exist";

            var reviewMockList = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Comment = "Amazing Driver",
                    Rating = 5,
                    Author = new User()
                    {

                    },
                    Recipient = new User()
                    {

                    }
                }
            };

            var reviewMock = reviewMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);

            var service = new ReviewService(dbMock.Object);

            var actualMessage = string.Empty;
            int id = 2;

            try
            {
                var result = await service.GetByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Review_By_Id()
        {
            string expectedMessage = "Amazing Driver";
            decimal expectedRating = 5;

            var reviewMockList = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Comment = "Amazing Driver",
                    Rating = 5,
                    Author = new User()
                    {

                    },
                    Recipient = new User()
                    {

                    }
                }
            };

            var reviewMock = reviewMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);

            var service = new ReviewService(dbMock.Object);

            var actualMessage = string.Empty;
            decimal actualRating = 0;
            int id = 1;

            var result = await service.GetByIdAsync(id);

            actualMessage = result.Comment;
            actualRating = result.Rating;

            Assert.AreEqual(expectedMessage, actualMessage);
            Assert.AreEqual(expectedRating, actualRating);
        }

        // Test when rating is above or below 0[] this is not fo rhere for creating but in UserServices
    }
}
