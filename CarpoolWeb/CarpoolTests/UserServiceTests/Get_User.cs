﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.UserServiceTests
{
    [TestClass]
    public class Get_User
    {
        private ICarsService carsService;
        private CarpoolDBContect fakeDb;
        private IUserService userService;
        private ITravelsService travelService;
        private ICloudinaryService cloudinaryService;

        // Get All Users Tests: 

        [TestMethod]
        public async Task Should_Throw_Exception_Get_All_Users()
        {
            var expectedMessage = "No users exist";

            var users = new List<User>();

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetAll();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_All_Users_Test()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var result = await service.GetAll();

            Assert.AreEqual(result.First().FirstName, expectedFirstName);
            Assert.AreEqual(result.First().LastName, expectedLastName);
            Assert.AreEqual(result.First().Username, expectedUserName);
        }

        // Get User by Id Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Get_User_By_Id_Invalid_Id()
        {
            var expectedMessage = "User with Id: 1 does not exist";

            var users = new List<User>();

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_User_By_Id()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;

            var result = await service.GetByIdAsync(id);

            Assert.AreEqual(result.FirstName, expectedFirstName);
            Assert.AreEqual(result.LastName, expectedLastName);
            Assert.AreEqual(result.Username, expectedUserName);
        }

        // Get User By Username Tests
        [TestMethod]
        public async Task Should_Throw_Exception_Get_User_By_Username()
        {
            var expectedMessage = "User with Username: failedUsername does not exist";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var userName = "failedUsername";
            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByUsernameAsync(userName);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_User_By_Username()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var result = await service.GetByUsernameAsync(expectedUserName);

            Assert.AreEqual(expectedFirstName, result.FirstName);
            Assert.AreEqual(expectedLastName, result.LastName);
            Assert.AreEqual(expectedUserName, result.Username);
        }

        // Get User By Email Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Get_User_By_Email()
        {
            var expectedMessage = "User with Email: test@mail.bg not found";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";
            var expectedEmail = "test@mail.bg";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0],
                    Email = expectedEmail
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.SearchByEmailAsync(expectedEmail);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task GetUserByEmail()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";
            var expectedEmail = "test@mail.bg";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0],
                    Email = expectedEmail
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var result = await service.SearchByEmailAsync(expectedEmail);

            Assert.AreEqual(expectedFirstName, result.First().FirstName);
            Assert.AreEqual(expectedLastName, result.First().LastName);
            Assert.AreEqual(expectedUserName, result.First().Username);
            Assert.AreEqual(expectedEmail, result.First().Email);
        }

        // Get User By Phone Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Get_User_By_Phone()
        {
            var expectedMessage = "User with Email: 0888888888 not found";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";
            var expectedEmail = "test@mail.bg";
            var expectedPhone = "0888888888";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0],
                    Email = expectedEmail,
                    PhoneNumber = "0999999999"
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.GetByPhoneAsync(expectedPhone);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_User_By_Phone()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";
            var expectedEmail = "test@mail.bg";
            var expectedPhone = "0888888888";

            var reviews = new List<Review>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0],
                    Email = expectedEmail,
                    PhoneNumber = expectedPhone
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var result = await service.GetByPhoneAsync(expectedPhone);

            Assert.AreEqual(expectedFirstName, result.First().FirstName);
            Assert.AreEqual(expectedLastName, result.First().LastName);
            Assert.AreEqual(expectedUserName, result.First().Username);
            Assert.AreEqual(expectedEmail, result.First().Email);
            Assert.AreEqual(expectedPhone, result.First().PhoneNumber);
        }

        // Get User By Rating Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_No_User_With_Rating()
        {
            var expectedMessage = "Users with Rating: 4.5 not found";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";
            var expectedEmail = "test@mail.bg";
            var expectedPhone = "0888888888";
            var expectedRating = 4.5M;

            var reviews = new List<Review>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0],
                    Email = expectedEmail,
                    PhoneNumber = "0999999999"
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.FilterByRatingAsync(expectedRating);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_User_By_Rating()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";
            var expectedEmail = "test@mail.bg";
            var expectedPhone = "0888888888";
            var expectedRating = 4.5M;

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0],
                    Email = expectedEmail,
                    PhoneNumber = expectedPhone,
                    RecipientReviews = new List<Review>
                    {
                        new Review
                        {
                            Id = 1,
                            Rating = 4.5M
                        }
                    }
                }
            };

            var reviews = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Rating = 4.5M,
                    Comment = "Amazing",
                    Author = new User(),
                    Recipient = new User(),
                    RecipientId = 1
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var result = await service.FilterByRatingAsync(expectedRating);

            Assert.AreEqual(expectedFirstName, result.First().FirstName);
            Assert.AreEqual(expectedLastName, result.First().LastName);
            Assert.AreEqual(expectedUserName, result.First().Username);
            Assert.AreEqual(expectedEmail, result.First().Email);
            Assert.AreEqual(expectedPhone, result.First().PhoneNumber);
            Assert.AreEqual(expectedRating, result.First().AverageRating);
        }

        // Get User's Car by UserId Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Cars_Invalid_User_Id_Exception()
        {
            var expectedMessage = "User with Id: 0 does not exist";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 0;

            try
            {
                var result = await service.SeeAllHisCarsAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Get_Cars_By_User_Id()
        {
            var expectedMessage = "User with Id: 0 does not have any cars";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            try
            {
                var result = await service.SeeAllHisCarsAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_Users_cars_By_User_Id()
        {
            var expectedRegistration = "OB8888CA";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    Owner = new User
                    {
                        Id = 1,
                        FirstName = "firstName",
                        LastName = "LastName",
                        UserName = "UserName",
                        Role = roles[0]
                    },
                    Registration = "OB8888CA"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "firstName",
                    LastName = "LastName",
                    UserName = "UserName",
                    Role = roles[0],
                    Cars = new List<Car>
                    {
                        new Car
                        {
                            Id = 1,
                            User = new User
                            {
                                Id = 1,
                                FirstName = "firstName",
                                LastName = "LastName",
                                UserName = "UserName",
                                Role = roles[0]
                            },
                            Registration = "OB8888CA",
                            Brand = "Ford",
                            Model = "Escort",
                            TotalSeats = 5,
                            CanSmoke = false,
                            PetsAllowed = false,
                            Color = "Green"
                        }
                    }
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var id = 1;

            var result = await service.SeeAllHisCarsAsync(id);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
        }

        // Get Users Author Reviews Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Author_Reviews_Invalid_User_Id()
        {
            var expectedMessage = "User with Id: 0 does not exist";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var reviews = new List<Review>
            {
                new Review
                {
                    Id = 1,
                    Rating = 5,
                    Recipient = new User(),
                    Author = new User(),
                    Comment = "Amazing"
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 0;

            try
            {
                var result = await service.SeeAllHisAuthorReviewsAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Author_Reviews_User_No_Reviews()
        {
            var expectedMessage = "User with Id: 0 does not have any author reviews";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var reviews = new List<Review>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            try
            {
                var result = await service.SeeAllHisAuthorReviewsAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_Users_Author_Reviews()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var expectedComment = "Amazing";
            var expectedRating = 5.0M;

            var reviews = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Rating = 5.0M,
                    Comment = "Amazing",
                    AuthorId = 1,
                    RecipientId = 2,
                    Author = new User()
                    {
                        Id = 1,
                        FirstName = expectedFirstName,
                        LastName = expectedLastName,
                        UserName = "Author",
                    },
                    Recipient = new User()
                    {
                        Id = 2,
                        FirstName = expectedFirstName,
                        LastName = expectedLastName,
                        UserName = "Recipient"
                    }
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            var result = await service.SeeAllHisAuthorReviewsAsync(id);

            Assert.AreEqual(expectedComment, result.First().Comment);
            Assert.AreEqual(expectedRating, result.First().Rating);
        }

        // Get Users Recipient Reviews Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Recipient_Reviews_Invalid_User_Id()
        {
            var expectedMessage = "User with Id: 0 does not exist";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var reviews = new List<Review>
            {
                new Review
                {
                    Id = 1,
                    Rating = 5,
                    Recipient = new User(),
                    Author = new User(),
                    Comment = "Amazing"
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 0;

            try
            {
                var result = await service.SeeAllHisRecipientReviewsAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Recipient_Reviews_User_No_Reviews()
        {
            var expectedMessage = "User with Id: 0 does not have any recipient reviews";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var reviews = new List<Review>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            try
            {
                var result = await service.SeeAllHisRecipientReviewsAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_Users_Recipient_Reviews()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var expectedComment = "Amazing";
            var expectedRating = 5.0M;

            var reviews = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Rating = 5.0M,
                    Comment = "Amazing",
                    AuthorId = 2,
                    RecipientId = 1,
                    Author = new User()
                    {
                        Id = 2,
                        FirstName = expectedFirstName,
                        LastName = expectedLastName,
                        UserName = "Author",
                    },
                    Recipient = new User()
                    {
                        Id = 1,
                        FirstName = expectedFirstName,
                        LastName = expectedLastName,
                        UserName = "Recipient"
                    }
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            var result = await service.SeeAllHisRecipientReviewsAsync(id);

            Assert.AreEqual(expectedComment, result.First().Comment);
            Assert.AreEqual(expectedRating, result.First().Rating);
        }

        // Get All User Author Requests Test:
        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Recipient_Requests_Invalid_User_Id()
        {
            var expectedMessage = "User with Id: 0 does not exist";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var requests = new List<Request>
            {
                new Request
                {
                    Id = 1,
                    Recipient = new User(),
                    Author = new User(),
                    Status = RequestEnum.Pending
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 0;

            try
            {
                var result = await service.SeeAllHisRecipientRequestAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Recipient_Requests_No_Requests()
        {
            var expectedMessage = "User with Id: 1 does not have any recipient requests";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var requests = new List<Request>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            try
            {
                var result = await service.SeeAllHisRecipientRequestAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_Users_Recipient_Requests()
        {
            var expectedMessage = "Pending";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var requests = new List<Request>()
            {
                new Request()
                {
                    Id = 1,
                    Recipient = new User()
                    {
                        Id = 1,
                        UserName = "Recipient",
                    },
                    RecipientId = 1,
                    Author = new User()
                    {
                        Id = 2,
                        UserName = "Author",
                    },
                    Status =  RequestEnum.Pending
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            var result = await service.SeeAllHisRecipientRequestAsync(id);

            Assert.AreEqual(expectedMessage, result.First().Status.ToString());
        }

        // Get All User Recipient Request Tests: 

        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Author_Requests_Invalid_User_Id()
        {
            var expectedMessage = "User with Id: 0 does not exist";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var requests = new List<Request>
            {
                new Request
                {
                    Id = 1,
                    Recipient = new User(),
                    Author = new User(),
                    Status = RequestEnum.Pending
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 0;

            try
            {
                var result = await service.SeeAllHisAuthorRequestAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Get_Users_Author_Requests_No_Requests()
        {
            var expectedMessage = "User with Id: 1 does not have any author requests";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var requests = new List<Request>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            try
            {
                var result = await service.SeeAllHisAuthorRequestAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }
        }

        [TestMethod]
        public async Task Get_Users_Author_Requests()
        {
            var expectedMessage = "Pending";

            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var requests = new List<Request>()
            {
                new Request()
                {
                    Id = 1,
                    Recipient = new User()
                    {
                        Id = 2,
                        UserName = "Recipient",
                    },
                    RecipientId = 2,
                    Author = new User()
                    {
                        Id = 1,
                        UserName = "Author",
                    },
                    AuthorId = 1,
                    Status =  RequestEnum.Pending
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            var result = await service.SeeAllHisAuthorRequestAsync(id);

            Assert.AreEqual(expectedMessage, result.First().Status.ToString());
        }

        [TestMethod]
        public async Task Get_User_Travels()
        {
            var expectedFirstName = "firstName";
            var expectedLastName = "LastName";
            var expectedUserName = "UserName";

            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Name = "Start"
                },
                new Address
                {
                    Id = 2,
                    Name = "End"
                }
            };

            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    StartLocation = addresses[0],
                    StartLocationId = 1,
                    Destination = addresses[1],
                    DestinationId = 2,
                    DepartureTime = DateTime.Now,
                    ArrivalTime = DateTime.Now,
                    Breaks = false,
                    AvailableSeat = 4,
                    Car = new Car
                    {
                        Id = 1,
                        Registration = "OB8888CA"
                    },
                    Status = new Status
                    {
                        Id = 1,
                        StatusType = "finished"
                    }
                }
            };

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = expectedFirstName,
                    LastName = expectedLastName,
                    UserName = expectedUserName,
                    Role = roles[0]
                }
            };

            var roleMock = roles.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;
            int id = 1;

            var result = await service.OwnTravelHistory(id);
        }

        [TestMethod]
        public async Task Should_Throw_Exception_User_Average_Rating_No_Ratings()
        {
            int id = 1;
            var expectedMessage = $"User with Id: {id} does not have any ratings";

            var firstName = "firstName";
            var lastName = "LastName";
            var userName = "UserName";

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = firstName,
                    LastName = lastName,
                    UserName = userName,
                }
            };

            var reviews = new List<Review>();

            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.AverageRatingAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}