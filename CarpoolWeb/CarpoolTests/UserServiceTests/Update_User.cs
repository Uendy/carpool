﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.UserServiceTests
{
    [TestClass]
    public class Update_User
    {
        private ICarsService carsService;
        private CarpoolDBContect fakeDb;
        private IUserService userService;
        private ITravelsService travelService;
        private ICloudinaryService cloudinaryService;

        // Update User Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Update_User_User_Id_Invalid()
        {
            var expectedMessage = "User with Id: 1 does not exist";

            var users = new List<User>();

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            try
            {
                var result = await service.UpdateAsync(id, "", "", "", "", "", "");
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Update_User_Username_Taken()
        {
            var username = "username";
            var expectedMessage = $"Username: {username} is already taken";

            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    UserName = username,
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            try
            {
                var result = await service.UpdateAsync(id, username, "", "", "", "", "");
            }
            catch (ArgumentException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Update_User_Email_Taken()
        {
            var email = "email@mail.bg";
            var expectedMessage = $"Email: {email} is already taken";

            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    Email = email,
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            try
            {
                var result = await service.UpdateAsync(id, "", "", "", "", email, "");
            }
            catch (ArgumentException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Update_User_Phone_Taken()
        {
            var phone = "0888888888";
            var expectedMessage = $"Phone Number: {phone} is already taken";

            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    PhoneNumber = phone,
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            try
            {
                var result = await service.UpdateAsync(id, "", "", "", "", "", phone);
            }
            catch (ArgumentException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Update_User_Null_Values()
        {
            var username = "username";
            var password = "password";
            var firstName = "first";
            var lastName = "last";
            var email = "email@mail.bg";
            var phone = "0888888888";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    UserName = username,
                    Password = password,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    PhoneNumber = phone,
                    Role = roles[0],
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var roleMock = roles.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            var result = await service.UpdateAsync(id, null, null, null, null, null, null);

            Assert.AreEqual(username, result.Username);
            Assert.AreEqual(password, result.Password);
            Assert.AreEqual(firstName, result.FirstName);
            Assert.AreEqual(lastName, result.LastName);
            Assert.AreEqual(email, result.Email);
            Assert.AreEqual(phone, result.PhoneNumber);
        }

        [TestMethod]
        public async Task Update_User_Test()
        {
            var username = "username";
            var password = "password";
            var firstName = "first";
            var lastName = "last";
            var email = "email@mail.bg";
            var phone = "0888888888";

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    RoleType = "Admin"
                }
            };

            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    UserName = "badName",
                    Password = "badPassword",
                    FirstName = "BadBoy",
                    LastName = "MarwaLoud",
                    Email = "French@win.bg",
                    PhoneNumber = "099999999",
                    Role = roles[0],
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var roleMock = roles.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Roles).Returns(roleMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            int id = 1;
            var actualMessage = string.Empty;

            var result = await service.UpdateAsync(id, username, password, firstName, lastName, email, phone);

            Assert.AreEqual(username, result.Username);
            Assert.AreEqual(password, result.Password);
            Assert.AreEqual(firstName, result.FirstName);
            Assert.AreEqual(lastName, result.LastName);
            Assert.AreEqual(email, result.Email);
            Assert.AreEqual(phone, result.PhoneNumber);
        }

        // Edit Review Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Edit_Review_No_Review()
        {
            var id = 1;
            var expectedMessage = $"User with Id: {id} has not written a review with Id: {id}";

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var reviews = new List<Review>();

            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            try
            {
                var result = await service.EditReviewAsync(id, id, "", 0.0M);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Edit_Review_Null_Values()
        {
            var expectedComment = $"Amazing";
            var expectedRating = 5.0M;

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var reviews = new List<Review>()
            {
                new Review()
                {
                    Id = 1,
                    AuthorId = 1,
                    Author = new User(),
                    RecipientId = 2,
                    Recipient = new User(),
                    Rating = 5.0M,
                    Comment = "Amazing"
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var id = 1;
            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            var result = await service.EditReviewAsync(id, id, null, 0.0M);

            Assert.AreEqual(expectedComment, result.Comment);
            Assert.AreEqual(expectedRating, result.Rating);
        }

        [TestMethod]
        public async Task Update_Review_Test()
        {
            var expectedComment = $"Amazing";
            var expectedRating = 5.0M;

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var reviews = new List<Review>()
            {
                new Review()
                {
                    Id = 1,
                    AuthorId = 1,
                    Author = new User(),
                    RecipientId = 2,
                    Recipient = new User(),
                    Rating = 3.0M,
                    Comment = "Swag"
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var id = 1;
            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            var result = await service.EditReviewAsync(id, id, expectedComment, expectedRating);

            Assert.AreEqual(expectedComment, result.Comment);
            Assert.AreEqual(expectedRating, result.Rating);
        }

        // Edit Request Tests: 

        [TestMethod]
        public async Task Should_Throw_Exception_Reply_Request_No_Request()
        {
            var id = 1;
            var expectedMessage = $"User with Id: {id} has not written a Request with Id: {id}";

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var requests = new List<Request>();

            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            try
            {
                var result = await service.EditRequestAsync(id, id, true);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Edit_Request_Accepted_Test()
        {
            var id = 1;

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var requests = new List<Request>()
            {
                new Request
                {
                    Id = 1,
                    Author = new User(),
                    AuthorId = 1,
                    Recipient = new User(),
                    RecipientId = 2,
                    Status = RequestEnum.Pending
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            var result = await service.EditRequestAsync(id, id, true);

            var expectedReply = "Accepted";

            Assert.AreEqual(expectedReply, result.Status.ToString());
        }

        [TestMethod]
        public async Task Edit_Request_Declined_Test()
        {
            var id = 1;

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var requests = new List<Request>()
            {
                new Request
                {
                    Id = 1,
                    Author = new User(),
                    AuthorId = 1,
                    Recipient = new User(),
                    RecipientId = 2,
                    Status = RequestEnum.Pending
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            var result = await service.EditRequestAsync(id, id, false);

            var expectedReply = "Declined";

            Assert.AreEqual(expectedReply, result.Status.ToString());
        }
    }
}