﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.UserServiceTests
{
    [TestClass]
    public class Delete_User
    {
        private ICarsService carsService;
        private CarpoolDBContect fakeDb;
        private IUserService userService;
        private ITravelsService travelService;
        private ICloudinaryService cloudinaryService;

        // Delete Review Requests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Delete_Review_No_Review()
        {
            var id = 1;
            var expectedMessage = $"User with Id: {id} has not written a review with Id: {id}";

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var reviews = new List<Review>();

            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            try
            {
                await service.DeleteReviewAsync(id, id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Delete_Review_Test()
        {
            var id = 1;

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var reviews = new List<Review>()
            {
                new Review
                {
                    Id = 1,
                    Rating = 5.0M,
                    Comment = "Amazing",
                    Author = new User(),
                    AuthorId = 1,
                    Recipient = new User(),
                    RecipientId = 2
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var reviewMock = reviews.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Reviews).Returns(reviewMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            await service.DeleteReviewAsync(id, id);

            Assert.AreEqual(true, reviews.First().IsDelete);
        }

        // Delete Request Tests: 
        [TestMethod]
        public async Task Should_Throw_Exception_Delete_Request_No_Request()
        {
            var id = 1;
            var expectedMessage = $"User with Id: {id} has not written a Request with Id: {id}";

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var requests = new List<Request>();

            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = requests.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            try
            {
                await service.DeleteRequestAsync(id, id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Delete_Request_Test()
        {
            var id = 1;

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "First",
                },
                new User
                {
                    Id = 2,
                    UserName = "Last",
                }
            };

            var request = new List<Request>()
            {
                new Request
                {
                    Id = 1,
                    Author = new User(),
                    AuthorId = 1,
                    Recipient = new User(),
                    RecipientId = 2,
                    Status = RequestEnum.Pending
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();
            var requestMock = request.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);
            dbMock.Setup(context => context.Requests).Returns(requestMock.Object);
            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            await service.DeleteRequestAsync(id, id);

            Assert.AreEqual(true, request.First().IsDelete);
        }

        // Delete User Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_No_User_By_Id()
        {
            int id = 1;
            var expectedMessage = $"User with Id: {id} does not exist";

            var users = new List<User>();

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);

            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            try
            {
                await service.DeleteOwnUserAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Delete_User_Test()
        {
            int id = 1;
            var expectedMessage = $"User with Id: {id} does not exist";

            var users = new List<User>()
            {
                new User
                {
                    Id = 1,
                    UserName = "PeshoTesta"
                }
            };

            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Users).Returns(userMock.Object);

            this.fakeDb = dbMock.Object;

            var fakeUserService = new Mock<IUserService>();
            this.userService = fakeUserService.Object;

            var service = new UserService(dbMock.Object, cloudinaryService);
            var actualMessage = string.Empty;

            await service.DeleteOwnUserAsync(id);

            Assert.AreEqual(true, users.First().IsDelete);
        }
    }
}