﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.CarServiceTests
{
    [TestClass]
    public class Get_Car
    {
        private readonly IUserService userService;

        // Get all cars tests:
        [TestMethod]
        public async Task Get_Cars_Exception()
        {
            string expectedMessage = "No cars exist";

            var carMockList = new List<Car>();

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            var actualMessage = string.Empty;
            try
            {
                var result = await service.GetAllAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Cars()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = true;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            var result = await service.GetAllAsync();

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }

        // Get Car by Id Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_Get_Car_By_Id()
        {
            var expectedMessage = "Car with Id: 2 does not exist";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            var id = 2;
            var actualMessage = string.Empty;

            try
            {
                await service.GetByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Car_By_Id()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = true;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            int id = 1;

            var result = await service.GetByIdAsync(id);

            Assert.AreEqual(expectedRegistration, result.Registration);
            Assert.AreEqual(expectedBrand, result.Brand);
            Assert.AreEqual(expectedModel, result.Model);
            Assert.AreEqual(expectedColor, result.Color);
            Assert.AreEqual(expectedCanSmoke, result.CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.PetsAllowed);
        }

        // Smoking Tests
        [TestMethod]
        public async Task Should_Throw_Exception_No_Smoking()
        {
            string expectedMessage = "Cars where you can not smoke are not found";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = true,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.FilterBySmokeAsync(false);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Smoking()
        {
            var expectedMessage = "Cars where you can smoke are not found";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.FilterBySmokeAsync(true);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Cars_Can_Smoke()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = true;
            var expectedPetsAllowed = true;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = true,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var result = await service.FilterBySmokeAsync(true);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }

        [TestMethod]
        public async Task Get_Cars_Can_Not_Smoke()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = true;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var result = await service.FilterBySmokeAsync(false);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }

        // Pets Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_No_Pets()
        {
            string expectedMessage = "Cars where you can bring pets are not found";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = true,
                    PetsAllowed = false,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.FilterByPetsAllowerdAsync(true);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Throw_Exception_Cars_With_Pets_Only()
        {
            var expectedMessage = "No cars found matching your query";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.FilterByPetsAllowerdAsync(false);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Cars_Can_Bring_Pets()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = true;
            var expectedPetsAllowed = true;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = true,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var result = await service.FilterByPetsAllowerdAsync(true);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }

        [TestMethod]
        public async Task Get_Cars_No_Pets_Allowed()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = false;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = false,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var result = await service.FilterByPetsAllowerdAsync(false);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }

        // Get Car by OwnerId Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_Owner_Has_No_Cars()
        {
            var expectedMessage = "User with Id: 1 does not have any cars";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 2,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = false,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            int userId = 1;
            var actualMessage = string.Empty;

            try
            {
                var cars = await service.GetByOwnerAsync(userId);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Car_By_Owner_Id()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = false;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = false,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            var id = 1;

            var result = await service.GetByOwnerAsync(id);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }

        // Multiple Query Tests:
        [TestMethod]
        public async Task Should_Throw_Exception_No_Cars_Match()
        {
            var expectedMessage = "No cars match your query";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = false,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var actualMessage = string.Empty;

            try
            {
                var result = await service.MultipleQueryFilterAsync(true, true);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Cars_Multiple_Query()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = false;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = false,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);

            var result = await service.MultipleQueryFilterAsync(false, false);

            Assert.AreEqual(expectedRegistration, result.First().Registration);
            Assert.AreEqual(expectedBrand, result.First().Brand);
            Assert.AreEqual(expectedModel, result.First().Model);
            Assert.AreEqual(expectedColor, result.First().Color);
            Assert.AreEqual(expectedCanSmoke, result.First().CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.First().PetsAllowed);
        }
    }
}