﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.CarServiceTests
{
    [TestClass]
    public class Delete_Car
    {
        private IUserService userService;
        // Delete Car Service Test Methods: 

        [TestMethod]
        public async Task Should_Throw_Exception_No_Car_By_Id_Delete()
        {
            var expectedMessage = "User with Id: 1 does not have car with Registration: OB7777CA";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, this.userService);
            var actualMessage = string.Empty;

            try
            {
                await service.DeteleOwnCarAsync(1, "OB7777CA");
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Delete_Car_Test()
        {
            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, this.userService);

            await service.DeteleOwnCarAsync(1, "OB8888CA");

            Assert.AreEqual(true, carMockList.First().IsDelete);
        }
    }
}
