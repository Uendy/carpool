﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.CarServiceTests
{
    [TestClass]
    public class Update_Car
    {
        private IUserService userService;

        // Update Car Methods:
        [TestMethod]
        public async Task Should_Throw_Exception_Update_Car_Not_Found()
        {
            var expectedMessage = "User with Id: 1 does not have a car with Registration: OB7777CA";

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            var actualMessage = string.Empty;

            try
            {
                var result = await service.UpdateAsync("OB7777CA", 1, 5, "Ford", "Escort", "Green", false, false);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Update_Specific_Car()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Fiesta";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = true;

            var carMockList = new List<Car>()
            {
                new Car
                {
                    Registration = "OB8888CA",
                    UserId = 1,
                    Id = 1,
                    Brand = "Ford",
                    Model = "Escort",
                    Color = "Green",
                    CanSmoke = false,
                    PetsAllowed = true,
                    TotalSeats = 5,
                    User = new User("test", "test", "test", "test", "test", "test", "test")
                }
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            var service = new CarsService(dbMock.Object, userService);
            var result = await service.UpdateAsync("OB8888CA", 1, 5, "Ford", "Fiesta", "Green", false, true);

            Assert.AreEqual(expectedRegistration, result.Registration);
            Assert.AreEqual(expectedBrand, result.Brand);
            Assert.AreEqual(expectedModel, result.Model);
            Assert.AreEqual(expectedColor, result.Color);
            Assert.AreEqual(expectedCanSmoke, result.CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, result.PetsAllowed);
        }
    }
}