﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;
using CarpoolService.Contracts;

namespace CarpoolTests.CarServiceTests
{
    [TestClass]
    public class Create_Car
    {
        // Create Car Services Tests: 
        private IUserService userService;

        [TestMethod]
        public async Task Create_Car_Test()
        {
            var expectedRegistration = "OB8888CA";
            var expectedBrand = "Ford";
            var expectedModel = "Escort";
            var expectedColor = "Green";
            var expectedCanSmoke = false;
            var expectedPetsAllowed = true;

            var users = new List<User>()
            {
                new User()
                {
                    Id = 1,
                    UserName = "Username",
                    Cars = new List<Car>()
                }
            };

            var carMockList = new List<Car>()
            {
            };

            var carMock = carMockList.AsQueryable().BuildMockDbSet();
            var userMock = users.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            dbMock.Setup(context => context.Users).Returns(userMock.Object);


            var service = new CarsService(dbMock.Object, this.userService);
            var result = await service.CreateCarAsync(expectedRegistration, 1, 5, expectedBrand, expectedModel, expectedColor, expectedCanSmoke, expectedPetsAllowed);

            Assert.AreEqual(expectedRegistration, carMockList[0].Registration);
            Assert.AreEqual(expectedBrand, carMockList[0].Brand);
            Assert.AreEqual(expectedModel, carMockList[0].Model);
            Assert.AreEqual(expectedColor, carMockList[0].Color);
            Assert.AreEqual(expectedCanSmoke, carMockList[0].CanSmoke);
            Assert.AreEqual(expectedPetsAllowed, carMockList[0].PetsAllowed);
        }

        // Test for when something fails like registration is too short or too long
    }
}
