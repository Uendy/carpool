﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;

namespace CarpoolTests.StatusServiceTests
{
    [TestClass]
    public class Get_Status
    {
        // Get All Statuses Service Tests: 
        [TestMethod]
        public async Task Get_Statuses_Exception()
        {
            string expectedMessage = "No statuses exist";

            var statusMockList = new List<Status>();

            var statusMock = statusMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);

            var service = new StatusService(dbMock.Object);
            var actualMessage = string.Empty;
            try
            {
                var result = await service.GetAllAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Statuses()
        {
            string expectedMessage = "Travelling";

            var statusMockList = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var statusMock = statusMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);

            var service = new StatusService(dbMock.Object);
            var actualMessage = string.Empty;
            var result = await service.GetAllAsync();
            foreach (var item in result)
            {
                actualMessage = item.StatusType;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        // Get Status By Id Service Tests: 

        [TestMethod]
        public async Task Get_Status_By_Id_Exception()
        {
            string expectedMessage = "Status with Id: 2 does not exist";

            var statusMockList = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var statusMock = statusMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);

            var service = new StatusService(dbMock.Object);
            var actualMessage = string.Empty;
            int id = 2;

            try
            {
                var result = await service.GetByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_Status_By_Id()
        {
            string expectedMessage = "Travelling";

            var statusMockList = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var statusMock = statusMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);

            var service = new StatusService(dbMock.Object);
            int id = 1;
            var result = await service.GetByIdAsync(id);
            var actualMessage = result.StatusType;

            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}