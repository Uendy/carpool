﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;

namespace CarpoolTests.StatusServiceTests
{
    [TestClass]
    public class Update_Status
    {
        [TestMethod]
        public async Task Should_ThrowException_When_Id_Does_Not_Exist()
        {
            string expectedMessage = "Status with Id: 2 does not exist";

            var statusMockList = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var statusMock = statusMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);

            var service = new StatusService(dbMock.Object);
            var actualMessage = string.Empty;
            int id = 2;
            string newStatusType = "Arrived";

            try
            {
                var result = await service.UpdateAsync(id, newStatusType);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Update_Status_StatusType()
        {
            string expectedMessage = "Arrived";

            var statusMockList = new List<Status>()
            {
                new Status
                {
                    Id = 1,
                    StatusType = "Travelling"
                }
            };

            var statusMock = statusMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Statuses).Returns(statusMock.Object);

            var service = new StatusService(dbMock.Object);
            int id = 1;
            string newStatusType = "Arrived";
            var result = await service.UpdateAsync(id, newStatusType);
            var actualMessage = result.StatusType;

            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}