﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;

namespace CarpoolTests.CountryServiceTests
{
    [TestClass]
    public class Get_Country
    {
        // Get All Country Service Tests:
        [TestMethod]
        public async Task Should_Return_EmptyList_Exctption()
        {
            string expectedMessage = "No countries exist";

            var countryMockList = new List<Country>();

            var countryMock = countryMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Countries).Returns(countryMock.Object);

            var service = new CountriesService(dbMock.Object);
            var actualMessage = string.Empty;
            try
            {
                var result = await service.GetCountriesAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Return_PreparedSelectList()
        {
            string expectedMessage = "Bulgaria";

            var countryMockList = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                }
            };

            var countryMock = countryMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Countries).Returns(countryMock.Object);

            var service = new CountriesService(dbMock.Object);
            var result = await service.GetCountriesAsync();
            string actualMessage = string.Empty;
            foreach (var item in result)
            {
                actualMessage = item.Name;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        // Get Country by Id Service Tests:
        [TestMethod]
        public async Task Should_Return_CountryById()
        {
            string expectedMessage = "Bulgaria";

            var countryMockList = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                }
            };

            var countryMock = countryMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Countries).Returns(countryMock.Object);

            var service = new CountriesService(dbMock.Object);
            int id = 1;
            var result = await service.GetCountriesByIdAsync(id);
            string actualMessage = result.Name;

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Return_Invalid_Id_Exception()
        {
            string expectedMessage = "Country with Id: 2 does not exist";

            var countryMockList = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                }
            };

            var countryMock = countryMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Countries).Returns(countryMock.Object);

            var service = new CountriesService(dbMock.Object);
            int id = 2;
            string actualMessage = string.Empty;
            try
            {
                var result = await service.GetCountriesByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}