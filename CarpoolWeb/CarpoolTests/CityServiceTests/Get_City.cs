﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CarpoolData.Models;
using CarpoolData.CarpoolDBContext;
using CarpoolService.ModelService;
using CarpoolService.Exception;

namespace CarpoolTests.CityServiceTests
{
    [TestClass]
    public class Get_City
    {

        // Get All City Service Tests:
        [TestMethod]
        public async Task Should_Return_EmptyList_Exctption()
        {
            string expectedMessage = "No Cities exist";

            var cityMockList = new List<City>();

            var cityMock = cityMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cities).Returns(cityMock.Object);

            var service = new CityService(dbMock.Object);
            var actualMessage = string.Empty;
            try
            {
                var result = await service.GetCitiesAsync();
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Return_List()
        {
            string expectedMessage = "Sofia";

            var cityMockList = new List<City>
            {
                new City
                {
                    Id = 1,
                    Country = new Country
                    {
                        Id = 1,
                        Name = "Bulgaria"
                    },
                    CountryId = 1,
                    Name = "Sofia"
                }
            };

            var city = cityMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cities).Returns(city.Object);

            var service = new CityService(dbMock.Object);
            var result = await service.GetCitiesAsync();
            string actualMessage = string.Empty;
            foreach (var item in result)
            {
                actualMessage = item.Name;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        // Get City By Id Service Tests:

        [TestMethod]
        public async Task Get_City_By_Id()
        {
            string expectedMessage = "Sofia";

            var cityMockList = new List<City>
            {
                new City
                {
                    Id = 1,
                    Country = new Country
                    { 
                        Id = 1,
                        Name = "Bulgaria"
                    },
                    CountryId = 1,
                    Name = "Sofia"
                }
            };

            var city = cityMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cities).Returns(city.Object);

            var service = new CityService(dbMock.Object);
            int id = 1;
            var result = await service.GetCityByIdAsync(id);
            string actualMessage = result.Name;

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Get_City_By_Id_Exception()
        {
            string expectedMessage = "City with Id: 2 does not exist";

            var cityMockList = new List<City>
            {
                new City
                {
                    Id = 1,
                    Country = new Country
                    {
                        Id = 1,
                        Name = "Bulgaria"
                    },
                    CountryId = 1,
                    Name = "Sofia"
                }
            };

            var city = cityMockList.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<CarpoolDBContect>();
            dbMock.Setup(context => context.Cities).Returns(city.Object);

            var service = new CityService(dbMock.Object);
            int id = 2;
            string actualMessage = string.Empty;
            try
            {
                var result = await service.GetCityByIdAsync(id);
            }
            catch (EntityNotFoundException exception)
            {
                actualMessage = exception.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}
