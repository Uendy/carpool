﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
  public  class AdminAddressDTO
    {
        public AdminAddressDTO(Address address)
        {
            this.Id = address.Id;
            this.Name = address.Name;
            this.City = address.City.Name;
            this.Travels = address.Travels;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public virtual ICollection<Travel> Travels { get; set; }
    }
}
