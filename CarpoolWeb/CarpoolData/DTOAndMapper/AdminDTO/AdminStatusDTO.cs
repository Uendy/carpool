﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
  public  class AdminStatusDTO
    {
        public AdminStatusDTO(Status status)
        {
            this.Id = status.Id;
            this.StatusType = status.StatusType;
            this.Travels = status.Travels;
        }
        public int Id { get; set; }
        public string StatusType { get; set; }
        public virtual ICollection<Travel> Travels { get; set; }
    }
}
