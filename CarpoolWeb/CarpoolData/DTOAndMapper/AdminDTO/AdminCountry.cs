﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
  public  class AdminCountry
    {
        public AdminCountry(Country country)
        {
            this.Id = country.Id;
            this.Name = country.Name;
            this.Cities = country.Cities;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<City> Cities { get; set; }

    }
}
