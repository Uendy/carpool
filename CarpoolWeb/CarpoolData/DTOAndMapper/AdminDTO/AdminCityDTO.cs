﻿using CarpoolData.Models;
using System.Collections.Generic;


namespace CarpoolData.DTOAndMapper.AdminDTO
{
    public class AdminCityDTO
    {
        public AdminCityDTO(City city)
        {
            this.Id = city.Id;
            this.Name = city.Name;
            this.Country = city.Country.Name;
            this.Addresses = city.Addresses;

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public ICollection<Address> Addresses { get; set;}
    }
}
