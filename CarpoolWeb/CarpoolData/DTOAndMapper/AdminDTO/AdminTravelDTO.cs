﻿using CarpoolData.Models;
using System;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
    public class AdminTravelDTO
    {
        public AdminTravelDTO(Travel travel)
        {
            this.Id = travel.Id;
            this.DepartureTime = travel.DepartureTime;
            this.AvailableSeat = travel.AvailableSeat;
            this.AvailableSeat = travel.AvailableSeat;
            this.Passenger = travel.Passenger;
            this.Breaks = travel.Breaks;
            this.StartLocation = travel.StartLocation.Name;
            this.Destination = travel.Destination.Name;
            this.Car = travel.Car.Registration;
            this.Status = travel.Status;

        }
        public int Id { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public int AvailableSeat { get; set; }
        public ICollection<User> Passenger { get; set; }
        public bool Breaks { get; set; }
        public string StartLocation { get; set; }
        public string Destination { get; set; }
        public string Car { get; set; }
        public Status Status { get; set; }

    }
}
