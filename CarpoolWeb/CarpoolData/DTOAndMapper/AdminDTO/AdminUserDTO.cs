﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper
{
  public  class AdminUserDTO
    {
        public AdminUserDTO(User admin)
        {
            this.Id = admin.Id;
            this.Username = admin.UserName;
            this.Password = admin.Password;
            this.FirstName = admin.FirstName;
            this.LastName = admin.LastName;
            this.Email = admin.Email;
            this.Role = admin.Role.RoleType;
            this.PhoneNumber = admin.PhoneNumber;
            this.AverageRating = admin.AverageRating;
            this.ProfileImage = admin.ProfileImage;
            this.Reviews = admin.AuthorReviews;
            this.Reviews = admin.RecipientReviews;
            this.TravelHistory = admin.TravelHistory;
            this.Cars = admin.Cars;

        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ProfileImage { get; set; }
        public string PhoneNumber { get; set; }
        public decimal AverageRating { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Travel> TravelHistory { get; set; }
        public virtual ICollection<Car> Cars { get; set; }

        public string Role { get; set; }

    }
}
