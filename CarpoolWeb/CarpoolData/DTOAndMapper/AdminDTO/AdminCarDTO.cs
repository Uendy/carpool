﻿using CarpoolData.Models;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
   public class AdminCarDTO
    {
        public AdminCarDTO(Car car)
        {
            this.Id = car.Id;
            this.Registration = car.Registration;
            this.Owner = car.Owner.FirstName;
            this.Owner = car.Owner.LastName;
            this.Owner = car.Owner.UserName;
            this.Owner = car.Owner.PhoneNumber;//TODO Dancho
            this.Brand = car.Brand;
            this.Model = car.Model;
            this.Color = car.Color;
            this.CanSmoke = car.CanSmoke;
            this.PetsAllowed = car.PetsAllowed;
            this.TotalSeats = car.TotalSeats;
        }
        public int Id { get; set; }
        public string Registration { get; set; }
        public string Owner { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public bool CanSmoke { get; set; }
        public bool PetsAllowed { get; set; }
        public int TotalSeats { get; set; }


    }
}
