﻿using CarpoolData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
  public  class AdminReviewDTO
    {
        public AdminReviewDTO(Review review)
        {
            this.Id = review.Id;
            this.Author = review.Author.FirstName;
            this.Author = review.Author.LastName;
            this.Author = review.Author.Password;
            this.Author = review.Author.Email;
            this.Author = review.Author.UserName;
            this.Recipient = review.Recipient.FirstName;
            this.Recipient = review.Recipient.LastName;
            this.Recipient = review.Recipient.Password;
            this.Recipient = review.Recipient.Email;
            this.Rating = review.Rating;
            this.Comment = review.Comment;


        }
        public int Id { get; set; }
        public string Author { get; set; }    
        public string Recipient { get; set; }
        public decimal Rating { get; set; }
        public string Comment { get; set; }

    }
}
