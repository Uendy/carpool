﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper.AdminDTO
{
    public class AdminRoleDTO
    {
        public AdminRoleDTO(Role role)
        {
            this.Id = role.Id;
            this.RoleType = role.RoleType;
            this.Users = role.Users;
        }
        public int Id { get; set; }
        public string RoleType { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
