﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper
{
    public class CityDTO
    {

        public CityDTO(City city)
        {

            this.Name = city.Name;
            this.Country = city.Country.Name;
            this.Addresses = city.Addresses;

        }
        public string Name { get; set; }
        public string Country { get; set; }
        public ICollection<Address> Addresses { get; set; }

    }

}
