﻿using CarpoolData.Models;


namespace CarpoolData.DTOAndMapper.UserDTO
{
  public class AddressDTO
    {
        public AddressDTO(Address address)
        {
            this.Name = address.Name;
            this.City = address.City.Name;
        }
        public string Name { get; set; }
        public string City { get; set; }
    }
}
