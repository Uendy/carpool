﻿
using CarpoolData.Models;

namespace CarpoolData.DTOAndMapper.UserDTO
{
   public class RoleDTO
    {
        public RoleDTO(Role role)
        {
            this.RoleType = role.RoleType;
        }
        public string RoleType { get; set; }
    }
}
