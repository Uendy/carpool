﻿using CarpoolData.Models;
using System.Collections.Generic;

namespace CarpoolData.DTOAndMapper
{
   public class CountryDTO
    {
        public CountryDTO(Country country)
        {

            this.Name = country.Name;
            this.Cities = country.Cities;
        }
        public string Name { get; set; }
        public virtual ICollection<City> Cities { get; set; }
    }
}
