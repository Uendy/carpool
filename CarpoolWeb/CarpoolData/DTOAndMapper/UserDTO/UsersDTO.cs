﻿using CarpoolData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolData.DTOAndMapper
{
  public  class UsersDTO
    {
        public UsersDTO(User user)
        {

            this.Username = user.UserName;
            this.Password = user.Password;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;
            this.Role = user.Role.RoleType;
            this.PhoneNumber = user.PhoneNumber;
            this.AverageRating = user.AverageRating;
            this.ProfileImage = user.ProfileImage;
            this.Reviews = user.AuthorReviews;
            this.TravelHistory = user.TravelHistory;
            this.Cars = user.Cars;
            this.ValidationCode = user.ValidationCode;


        }
        public string ValidationCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ProfileImage { get; set; }
       public string PhoneNumber { get; set; }
        public decimal AverageRating { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Travel> TravelHistory { get; set; }
        public virtual ICollection<Car> Cars { get; set; }

        public string Role { get; set; }
    }
}
