﻿using CarpoolData.Models;
using System;

namespace CarpoolData.DTOAndMapper
{
   public class TravelDTO
    {
        public TravelDTO(Travel travel)
        {
            this.DepartureTime = travel.DepartureTime;
            this.AvailableSeat = travel.AvailableSeat;
            this.AvailableSeat = travel.AvailableSeat;
            this.Breaks = travel.Breaks;
            this.StartLocation = travel.StartLocation.Name;
            this.Destination = travel.Destination.Name;
            this.Car = travel.Car.Registration;
            this.Status = travel.Status.StatusType;

        }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public int AvailableSeat { get; set; } //TODO max lenght ? 
        public bool Breaks { get; set; }
        public string StartLocation { get; set; }
        public string Destination { get; set; }
        public string Car { get; set; }
        public string Status { get; set; }
    }
}

