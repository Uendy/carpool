﻿using CarpoolData.Models;

namespace CarpoolData.DTOAndMapper
{
    public class ReviewDTO
    {
        public ReviewDTO(Review review)
        {
            this.Author = review.Author.UserName;
            this.Recipient = review.Recipient.UserName;
            this.Rating = review.Rating;
            this.Comment = review.Comment;
        }
        public string Author { get; set; }
        public string Recipient { get; set; }
        public decimal Rating { get; set; }
        public string Comment { get; set; }
    }
}

