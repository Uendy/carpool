﻿using CarpoolData.Models;

namespace CarpoolData.DTOAndMapper
{
    public class CarDTO
    {
        public CarDTO(Car car)
        {
            this.Registration = car.Registration;
            this.Owner = car.User.UserName;
            this.Brand = car.Brand;
            this.Model = car.Model;
            this.Color = car.Color;
            this.CanSmoke = car.CanSmoke;
            this.PetsAllowed = car.PetsAllowed;
        }
        public string Registration { get; set; }
        public string Owner { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public bool CanSmoke { get; set; }
        public bool PetsAllowed { get; set; }
    }
}
