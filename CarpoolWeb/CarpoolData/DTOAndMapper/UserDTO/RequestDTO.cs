﻿using CarpoolData.Models;

namespace CarpoolData.DTOAndMapper.UserDTO
{
    public class RequestDTO
    {
        public RequestDTO(Request request)
        {
            this.Author = request.Author.UserName;
            this.Recipient = request.Recipient.UserName;
            this.Status = request.Status.ToString();
        }

        public string Author { get; set; }
        public string Recipient { get; set; }
        public string Status { get; set; }
    }
}