﻿using CarpoolData.Models;


namespace CarpoolData.DTOAndMapper.UserDTO
{
   public class StatusDTO
    {
        public StatusDTO(Status status)
        {
            this.StatusType = status.StatusType;
        }
        public string StatusType { get; set; }
    }
}
