﻿using CarpoolData.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace CarpoolData.Seed
{
    public static class ModelBuilderExtension
    {
        static ModelBuilderExtension()
        {
            Users = new List<User>
            {
                new User()
                {
                  Id= 1,
                UserName = "Plamen",
                Password = "Smotan",
                RoleId = 1,
                FirstName = "Plamen",
                LastName = "Haritonov",
                Email = "plamen@mail.com",
                PhoneNumber="0876855105",
                TravelId=1
                },
                new User()
                {
                     Id= 2,
                UserName = "Alex",
                Password = "Shefa",
                RoleId = 1,
                FirstName = "Alexander",
                LastName = "Beshkov",
                Email = "alex@mail.com",
                PhoneNumber="0872855105",
                TravelId=2
                }

            };
            Travels = new List<Travel>
            {
                new Travel
                {
                    Id= 1,
                    StartLocationId= 1,
                    DepartureTime=DateTime.Now.AddDays(1),
                    ArrivalTime=DateTime.Now.AddDays(1).AddHours(2),
                    StatusId=1,
                    CarId=1,
                    AvailableSeat=5,
                    DestinationId=2,

                    Breaks=false,
                  
                },
                 new Travel
                {
                    Id= 2,
                    StartLocationId= 2,
                    DepartureTime=DateTime.Now,
                    ArrivalTime=DateTime.Now.AddDays(1).AddHours(1),
                    StatusId=2,
                    CarId=2,
                    AvailableSeat=5,
                    DestinationId=1,
                    Breaks=false
                },
            };
            Cars = new List<Car>()
            {
                new Car()
                {
                    Id = 1,
                    UserId=1,
                    TotalSeats = 7,
                    TravelId = 1,
                    Brand = "Mercedes",
                    Model = "B180",
                    Color = "Silver",
                   // Owner = 1
                    CanSmoke = false,
                    PetsAllowed = true,
                    Registration = "OB8848CB"
                },
                new Car()
                {
                    Id = 2,
                    UserId=2,
                    TotalSeats = 5,
                    TravelId = 2,
                    Brand = "Audi",
                    Model = "R8",
                    Color = "Red",
                   // Owner = 2,
                    CanSmoke = true,
                    PetsAllowed = false,
                    Registration = "A3516BH"
                }
            };
            Roles = new List<Role>()
            {
                new Role()
                {
                    Id = 1,
                    RoleType = "Admin"
                },
                new Role()
                {
                    Id = 2,
                    RoleType = "User"
                },
                new Role()
                {
                    Id = 3,
                    RoleType = "Anonymous"
                },
                new Role()
                {
                    Id = 4,
                    RoleType = "Blocked"
                }
            };
            Status = new List<Status>()
            {
                new Status()
                {
                    Id = 1,
                    StatusType= "Traveling to destination"
                },
                new Status()
                {
                    Id = 2,
                    StatusType= "On the way"
                },
               new Status()
                {
                    Id = 3,
                    StatusType= "Arrived"
                },

            };
            Cities = new List<City>
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId=1,

                },
                new City()
                {
                    Id = 2,
                    Name = "Berlin",
                   CountryId=2,
                },
                new City()
                {
                   Id = 3,
                   Name = "Barcelona",
                   CountryId =3,
                },
                new City()
                {
                    Id = 4,
                    Name = "Rome",
                    CountryId=4,

                }
            };
            Addresses = new List<Address>
            {
                new Address()
                {
                   Id = 1,
                    Name = "Ivan Vazov 20",
                    CityId = 1
                },
                new Address()
                {
                    Id = 2,
                    Name = "Genslerstrasse 23",
                    CityId = 2
                },
                new Address()
                {
                    Id = 3,
                    Name = "Enric Granados 5",
                    CityId = 3
                },
                new Address()
                {
                    Id = 4,
                    Name = "Antonio Beccadelli 40",
                    CityId = 4
                }
            };
            Countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"

                },
                new Country()
                {
                    Id = 2,
                   Name = "Germany"
                },
                new Country()
                {
                    Id = 3,
                    Name = "Spain"
                },
                new Country()
                {
                    Id = 4,
                    Name = "Italy"
                }
            };
            Reviews = new List<Review>
            {
                new Review
                {
                    Id=1,
                    AuthorId=1,
                   RecipientId=2,
                   Comment="Great guest",
                   Rating=5

                },
                new Review
                {
                    Id=2,
                    AuthorId=1,
                   RecipientId=2,
                   Comment="Great guest",
                   Rating=5

                },
            };
            Requests = new List<Request>
            {
                new Request
                {
                    Id = 1,
                    AuthorId = 1,
                    RecipientId = 2,
                    TravelId = 1
                }
            };
        }

       public static List<User> Users { get; }
        public static List<Travel> Travels { get; }
        public static List<Car> Cars { get; }
        public static List<Role> Roles { get; }
        public static List<Status> Status { get; }
        public static List<City> Cities { get; }
        public static List<Address> Addresses { get; }
        public static List<Country> Countries { get; }
        public static List<Review> Reviews { get; }
        public static List<Request> Requests { get; }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>().HasData(Addresses);
            modelBuilder.Entity<Travel>().HasData(Travels);
            modelBuilder.Entity<User>().HasData(Users);
            modelBuilder.Entity<Car>().HasData(Cars);
            modelBuilder.Entity<Role>().HasData(Roles);
            modelBuilder.Entity<Status>().HasData(Status);
            modelBuilder.Entity<City>().HasData(Cities);
            modelBuilder.Entity<Country>().HasData(Countries);
            modelBuilder.Entity<Review>().HasData(Reviews);
            modelBuilder.Entity<Request>().HasData(Requests);
        }
    }
}
