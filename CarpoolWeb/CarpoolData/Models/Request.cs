﻿using System;

namespace CarpoolData.Models
{
    public class Request : Entity
    {
        public Request()
        { }
        public Request(int authorId, int recipientId, int travelId)
            : base()
        {
            this.AuthorId = authorId;
            this.RecipientId = recipientId; 
            this.TravelId = travelId;
            this.Status = RequestEnum.Pending;

            // maybe a comment = Communication through SignalR
            // User needs 2 more properties for requestSent and requestRecieved
        }

        public User Author { get; set; }
        public int AuthorId { get; set; }
        public User Recipient { get; set; } // Driver
        public int RecipientId { get; set; }
        public RequestEnum Status { get; set; } // Pending, Accepted, Rejected
        public Travel Travel { get; set; }
        public int TravelId { get; set; }
    }
}