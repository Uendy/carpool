﻿
using System.Collections.Generic;

namespace CarpoolData.Models
{
    public class Address:LocationEntity
    {
        public Address()
        {
            this.Travels = new HashSet<Travel>();
        }
        public int CityId { get; set; }
        public City City { get; set; }
        public virtual ICollection<Travel> Travels { get; set; }
    }
}
