﻿namespace CarpoolData.Models
{
    public abstract class LocationEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
