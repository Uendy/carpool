﻿using System.ComponentModel.DataAnnotations;

namespace CarpoolData.Models
{
    public class Car : Entity
    {
        public Car()
        { 
        }

        public Car(string registration, int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed, User owner)
            :base()
        {
            this.Registration = registration;
            this.UserId = userId;
            this.TotalSeats = totalSeats;
            this.Brand = brand;
            this.Model = model;
            this.Color = color;
            this.CanSmoke = canSmoke;
            this.PetsAllowed = petsAllowed;
            this.Owner = owner;
        }
        [Required, MinLength(6), MaxLength(10)]
        public string Registration { get; set; }
        [Required]
        public User Owner { get; set; }
        [Required]
        public int TotalSeats { get; set; }
        [Required]
        public string Brand { get; set; }
        public string Model { get; set; }
        [Required]
        public string Color { get; set; }
        public bool CanSmoke { get; set; }
        public bool PetsAllowed { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int? TravelId { get; set; }
        public Travel Travel { get; set; }
    }
}
