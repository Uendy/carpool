﻿namespace CarpoolData.Models
{
    public enum RequestEnum
    {
        Pending,
        Accepted,
        Declined
    }
}