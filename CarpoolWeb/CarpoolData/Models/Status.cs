﻿
using System.Collections.Generic;

namespace CarpoolData.Models
{
   public class Status
    {
        public Status()
        {
            this.Travels = new HashSet<Travel>();
        }
        public int Id { get; set; }
        public string StatusType { get; set; }
        public virtual  ICollection<Travel> Travels { get; set; }
    }
}
