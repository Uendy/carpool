﻿using System.Collections.Generic;

namespace CarpoolData.Models
{
    public class Role
    {
        public Role()
        {
            this.Users = new HashSet<User>();
        }
        public int Id { get; set; }
        public string RoleType { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
