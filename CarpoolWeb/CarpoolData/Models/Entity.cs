﻿using System;

namespace CarpoolData.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime DeleteOn { get; set; }
    }
}
