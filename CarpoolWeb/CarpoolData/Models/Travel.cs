﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarpoolData.Models
{
    public class Travel:Entity
    {
        public Travel()
        {
            this.Users = new HashSet<User>();
        }

        public Travel(DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId) 
            : base() 
        {
            this.DepartureTime = departureTime;
            this.ArrivalTime = arrivalTime;
            this.AvailableSeat = availableSeats;
            this.Breaks = breaks;
            this.StartLocationId = startLocationId;
            this.DestinationId = destinationLocationId;
            this.CarId = carId;
            this.StatusId = statusId;
        }

        [Required]
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        [Required]
        public int AvailableSeat { get; set; } //TODO max lenght ? 
        public ICollection<User> Passenger { get; set; }
        public bool Breaks { get; set; }
        public int StartLocationId { get; set; }
        [Required]
        public Address StartLocation { get; set; }
        public int DestinationId { get; set; }
        [Required]
        public Address Destination { get; set; }
        public int CarId { get; set; }
        public Car Car { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
