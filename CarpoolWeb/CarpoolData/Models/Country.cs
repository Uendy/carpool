﻿
using System.Collections.Generic;

namespace CarpoolData.Models
{
   public class Country:LocationEntity
    {
        public Country()
        {
            this.Cities = new HashSet<City>();
        }
        public virtual ICollection<City> Cities { get; set; }
    }
}
