﻿using System.ComponentModel.DataAnnotations;

namespace CarpoolData.Models
{
    public class Review : Entity
    {
        public Review()
        { 
        }

        public Review(int authorId, int recipientId, string comment, decimal rating)
            : base()
        {
            this.AuthorId = authorId;
            this.RecipientId = recipientId;
            this.Comment = comment;
            this.Rating = rating;
        }


        public User Author { get; set; }
        public User Recipient { get; set; }
        [Required]
        [Range(0.0, 5.0, ErrorMessage = "The field {0} must be greater than {1} and smaller than {2}.")]
        public decimal Rating { get; set; }
        [MinLength(2),MaxLength(200)]
        public string Comment { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int RecipientId { get; set; }

    }
}
