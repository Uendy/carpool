﻿using System.Collections.Generic;


namespace CarpoolData.Models
{
   public  class City:LocationEntity
    {
        public City()
        {
            this.Addresses = new HashSet<Address>();
        }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}
