﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarpoolData.Models
{
   public class User:Entity
    {
        public User()
        {
            this.TravelHistory = new HashSet<Travel>();
            this.AuthorReviews = new HashSet<Review>();
            this.RecipientReviews = new HashSet<Review>();
            this.AuthorRequests = new HashSet<Request>();
            this.RecipientRequests = new HashSet<Request>();
            this.Cars = new HashSet<Car>();
        }

        public User(string userName, string password, string firstName, string lastName, string email, string phoneNumber,string validationString)
            : base()
        {
            this.UserName = userName;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
            this.PhoneNumber = phoneNumber;
            this.ValidationCode = validationString;
            this.RoleId = 2;
        }

        [Required, MinLength(2), MaxLength(20)]
        public string UserName { get; set; }
        [Required, MinLength(8), RegularExpression(@"^(?=.*[a - z])(?=.*[A - Z])(?=.*\d)(?=.*[^\da - zA - Z]).{8,}$)")]
        public string Password { get; set; }
        [Required, MinLength(2), MaxLength(20)]
        public string  FirstName { get; set; }
        [Required, MinLength(2), MaxLength(20)]
        public string LastName { get; set; }
        [Required,EmailAddress]
        public string Email { get; set; }
        [Required, RegularExpression(@"^\(?\d{3}?\)??-??\(?\d{3}?\)?? -??\(?\d{ 4}?\)?? -?$")]
        public string PhoneNumber { get; set; } 
        public decimal AverageRating { get; set; }
        public string ProfileImage { get; set; }
        public string ValidationCode { get; set; }
        // Travel and TravelId are relations that mess up the DB, so that you can have multiple travels (not just current), this is not a property of the user.It is a different entity that the user has a relationship to. 
        // User has his cars, from cars you get their history of travel and from there you get passangers which is the many to many table created.

        // Bad idea to store image in DB, but we can store a select number of iamges in wwwroot and let user choose from them, but best practice is an object (cloud) storage like amazon S3

        // Learn: environment variables for stuff like connection string
        public int? TravelId { get; set; }
        public Travel Travel { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public virtual ICollection<Review> AuthorReviews { get; set; }
        public virtual ICollection<Review> RecipientReviews { get; set; }
        public virtual ICollection<Request> AuthorRequests { get; set; }
        public virtual ICollection<Request> RecipientRequests { get; set; }
        public virtual ICollection<Travel> TravelHistory { get; set; }
        public virtual ICollection<Car> Cars { get; set; }

    }
}
