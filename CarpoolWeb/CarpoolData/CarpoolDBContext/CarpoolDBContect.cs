﻿using CarpoolData.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using CarpoolData.Seed;

namespace CarpoolData.CarpoolDBContext
{
   public class CarpoolDBContect : DbContext
    {
        public CarpoolDBContect()
        {

        }
        public CarpoolDBContect(DbContextOptions options)
            :base(options)
        {
        }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Travel> Travels { get; set; }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<Request> Requests { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer("Server=.; DataBase=Carpool;Integrated Security=true;");
        //    }
        //    //base.OnConfiguring(optionsBuilder);
        //    base.OnConfiguring(optionsBuilder);
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>()
            .HasIndex(c => new { c.Email })
            .IsUnique(true);
            modelBuilder.Entity<User>()
            .HasIndex(c => new { c.PhoneNumber })
            .IsUnique(true);
            modelBuilder.Entity<User>()
            .HasIndex(c => new { c.UserName })
            .IsUnique(true);
            modelBuilder.Entity<Car>()
            .HasIndex(c => new { c.Registration })
            .IsUnique(true);

            modelBuilder.Entity<Car>().HasOne(x => x.User).WithMany(x => x.Cars).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<Travel>().HasOne(x => x.Car.Owner);
            //modelBuilder.Entity<Travel>().HasMany(x => x.Passenger)
            modelBuilder.Entity<User>().HasOne(x => x.Travel).WithMany(x => x.Users).HasForeignKey(x => x.TravelId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().HasOne(x => x.Role).WithMany(x => x.Users).HasForeignKey(x => x.RoleId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().HasMany(x => x.AuthorReviews).WithOne(x => x.Author).HasForeignKey(x => x.AuthorId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().HasMany(x => x.RecipientReviews).WithOne(x => x.Recipient).HasForeignKey(x => x.RecipientId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Travel>().HasOne(x => x.Status).WithMany(x => x.Travels).HasForeignKey(x => x.StatusId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Travel>().HasOne(x => x.Car).WithOne(x => x.Travel).HasForeignKey<Car>(x => x.TravelId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Address>().HasOne(x => x.City).WithMany(x => x.Addresses).HasForeignKey(x => x.CityId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<City>().HasOne(x => x.Country).WithMany(x => x.Cities).HasForeignKey(x => x.CountryId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Travel>().HasOne(x => x.StartLocation).WithMany(x => x.Travels).HasForeignKey(x => x.StartLocationId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().HasMany(x => x.AuthorRequests).WithOne(x => x.Author).HasForeignKey(x => x.AuthorId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().HasMany(x => x.RecipientRequests).WithOne(x => x.Recipient).HasForeignKey(x => x.RecipientId).OnDelete(DeleteBehavior.Restrict);
             //modelBuilder.Entity<Request>().HasOne(x => x.Travel).WithMany(x => x.)



            modelBuilder.Entity<User>().HasQueryFilter(x => !x.IsDelete);
            modelBuilder.Entity<Travel>().HasQueryFilter(x => !x.IsDelete);
            modelBuilder.Entity<Car>().HasQueryFilter(x => !x.IsDelete);
            modelBuilder.Entity<Review>().HasQueryFilter(x => !x.IsDelete);
            modelBuilder.Entity<Request>().HasQueryFilter(x => !x.IsDelete);
    
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();

        }
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDelete"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDelete"] = true;
                        break;
                }
            }
        }
    }
}
