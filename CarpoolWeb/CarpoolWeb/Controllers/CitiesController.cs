﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

using CarpoolService.Contracts;
using CarpoolService.Exception;

namespace CarpoolWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CitiesController : ControllerBase
    {
        private readonly ICityService citiesService;
        public CitiesController(ICityService cities)
        {
            this.citiesService = cities;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets all Cities",
            Description = "Gets all the Cities and the useful information for the user.")]
        public async Task<IActionResult> GetCities()
        {
            try
            {
                var cities = await this.citiesService.GetCitiesAsync();
                return this.Ok(cities);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get City by Id",
            Description = "Get specific City by Id property")]
        public async Task<IActionResult> GetCitiesId([FromRoute] int id)
        {
            try
            {
                var city = await this.citiesService.GetCityByIdAsync(id);
                return this.Ok(city);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }
    }
}