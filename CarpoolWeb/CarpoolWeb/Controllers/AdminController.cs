﻿//using CarpoolData.Models;
//using CarpoolService.Contracts;
//using CarpoolService.Exception;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Threading.Tasks;
//using RouteAttribute = Microsoft.AspNetCore.Components.RouteAttribute;

//namespace CarpoolWeb.Controllers
//{
//    [ApiController]
//    [Route("api/[controller]")]
//    public class AdminController :ControllerBase
//    {
//        private readonly IAdminsService adminsService;
//        public AdminController(IAdminsService adminsService)
//        {
//            this.adminsService = adminsService;
//        }

//        [HttpGet("/user/{id}")]
//        public async Task<IActionResult> GetUserByIdAsync(int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetUserByIdAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/users")]
//        public async Task<IActionResult> GetAllUsersAsync()
//        {
//            try
//            {
//                var admin = await this.adminsService.GetAllUsers();
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/username")]
//        public async Task<IActionResult> GetByUsernameAsync([FromHeader] string username)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetUserByUsernameAsync(username);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPost("")]
//        public async Task<IActionResult> CreateUserAsync([FromBody] User user)
//        {
//            try
//            {
//                var admin = await this.adminsService.CreateUserAsync(user);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("search/{email}")]
//        public async Task<IActionResult> SearchUserByEmailAsync([FromRoute] string email)
//        {
//            try
//            {
//                var admin = await this.adminsService.SearchUserByEmailAsync(email);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("search/phone/{phone}")]
//        public async Task<IActionResult> GetUserByPhoneAsync([FromRoute] string phone)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetUserByPhoneAsync(phone);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("Filter/rating")]
//        public async Task<IActionResult> FilterByRatingAsync([FromQuery] decimal rating)
//        {

//            try
//            {
//                var admin = await this.adminsService.FilterUsersByRatingAsync(rating);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("search")]
//        public async Task<IActionResult> MultipleQueryFilterUsersAsync([FromQuery] string firstname, string lastname, string email, string phone, string username)
//        {

//            try
//            {
//                var admin = await this.adminsService.MultipleQueryFilterUsersAsync(firstname, lastname, email, phone, username);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpPut("{id}")]
//        public async Task<IActionResult> UpdateUserAsync(int id, [FromBody] User user)
//        {
//            try
//            {

//                var admin = await this.adminsService.UpdateUserAsync(id, user);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteOwnUserAsync(int id)
//        {
//            try
//            {
//                await this.adminsService.DeleteUserAsync(id);
//                return this.Ok("Successfully deleted");

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("{id}/cars")]
//        public async Task<IActionResult> GetUserCarsAsync(int id)
//        {

//            try
//            {
//                var admin = await this.adminsService.GetAllUsersCarsAsync(id);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("{id}/review")]
//        public async Task<IActionResult> GetUserReviewAsync(int id)
//        {

//            try
//            {
//                var reviews = await this.adminsService.GetUsersReviewsAsync(id);
//                return this.Ok(reviews);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("{id}/rating")]
//        public async Task<IActionResult> GetAverageRatingAsync(int id)
//        {

//            try
//            {
//                var rating = await this.adminsService.GetUserAverageRatingAsync(id);
//                return this.Ok(rating);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpPut("{id}/review")]
//        public async Task<IActionResult> EditReviewAsync([FromBody] Review review, int id, int reviewId)
//        {
//            try
//            {
//                var reviews = await this.adminsService.EditUsersReviewAsync(id, reviewId, review);
//                return this.Ok(reviews);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpDelete("{userId}/review/{reviewId}")]
//        public async Task<IActionResult> DeleteCommentAsync(int userId, int reviewId)
//        {
//            try
//            {
//                await this.adminsService.DeleteUsersCommentAsync(userId, reviewId);
//                return this.Ok("Successfully deleted");

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("{id}/user/history")]
//        public async Task<IActionResult> OwnerTravelHistoryAsync(int id)
//        {

//            try
//            {
//                var admin = await this.adminsService.GetUsersTravelHistory(id);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpPost("car/")]
//        public async Task<IActionResult> CreateCarAsync([FromBody] Car car)
//        {
//            try
//            {
//                var admin = await this.adminsService.CreateCarAsync(car);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("all/cars/")]
//        public async Task<IActionResult> GetCarsAsync()
//        {
//            try
//            {
//                var admin = await this.adminsService.GetAllCarsAsync();
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("{id}/car/")]
//        public async Task<IActionResult> GetCarByIdAsync(int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetCarByIdAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("{id}/owner/car")]
//        public async Task<IActionResult> GetCarsByOwnerAsync([FromHeader] int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetCarsByOwnerAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("filter/smoke")]
//        public async Task<IActionResult> FilterCarsBySmokeAsync([FromQuery] bool smoke)
//        {

//            try
//            {
//                var admin = await this.adminsService.FilterCarsBySmokeAsync(smoke);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("filter/pets")]
//        public async Task<IActionResult> FilterCarsByPetsAllowerdAsync([FromQuery] bool pets)
//        {

//            try
//            {
//                var admin = await this.adminsService.FilterCarsByPetsAllowerdAsync(pets);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("search/pets/smoke/")]
//        public async Task<IActionResult> MultipleQueryFilterCarsAsync([FromQuery] bool PetsAllowed, bool CanSmoke)
//        {

//            try
//            {
//                var admin = await this.adminsService.MultipleQueryFilterCarsAsync(PetsAllowed, CanSmoke);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpPut("/car/{id}")]
//        public async Task<IActionResult> UpdateCarAsync(int id, [FromBody] Car car)
//        {
//            try
//            {

//                var admin = await this.adminsService.UpdateCarAsync(id, car);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpDelete("car")]
//        public async Task<IActionResult> DeteleCarAsync(int car)
//        {
//            try
//            {
//                await this.adminsService.DeteleCarAsync(car);
//                return this.Ok("Successfully deleted");

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return NotFound(ex.Message);

//            }
//        }
//        [HttpGet("/statuses")]
//        public async Task<IActionResult> GetStatus()
//        {
//            try
//            {
//                var admin = await this.adminsService.GetAllStatusAsync();
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/status/{Id}")]
//        public async Task<IActionResult> GetStatusById(int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetStatusByIdAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPut("/status/{id}")]
//        public async Task<IActionResult> UpdateStatusAsync(int id, [FromBody] Status status)
//        {
//            try
//            {

//                var admin = await this.adminsService.UpdateStatusAsync(id, status);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/reviews")]
//        public async Task<IActionResult> GetReview()
//        {
//            try
//            {
//                var admin = await this.adminsService.GetAllReviewsAsync();
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/review/{Id}")]
//        public async Task<IActionResult> GetReviewId(int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetReviewByIdAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPut("/review/{id}")]
//        public async Task<IActionResult> UpdateReviewAsync(int id, [FromBody] Review review)
//        {
//            try
//            {

//                var admin = await this.adminsService.UpdateReviewAsync(id, review);
//                return this.Ok(admin);

//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/cities/")]
//        public async Task<IActionResult> GetCities()
//        {
//            try
//            {
//                var admin = await this.adminsService.GetAllCitiesAsync();
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/cities/{Id}")]
//        public async Task<IActionResult> GetCitiesId(int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetCityByIdAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {

//                return this.StatusCode(500, ex.Message);

//            }
//        }
//        [HttpGet("/countries/")]
//        public async Task<IActionResult> GetAllCountriesAsync()
//        {
//            try
//            {
//                var admin = await this.adminsService.GetAllCountriesAsync();
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {

//                return this.NotFound(ex.Message);

//            }
//            catch (Exception ex)
//            {
//                return this.StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("/countries/{Id}")]
//        public async Task<IActionResult> GetCountryByIdAsync(int id)
//        {
//            try
//            {
//                var admin = await this.adminsService.GetCountryByIdAsync(id);
//                return this.Ok(admin);
//            }
//            catch (EntityNotFoundException ex)
//            {
//                return this.NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return this.StatusCode(500, ex.Message);
//            }
//        }
//    }
//}
