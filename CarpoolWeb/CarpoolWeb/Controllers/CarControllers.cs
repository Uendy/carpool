﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using CarpoolService.Exception;
using CarpoolService.Contracts;
using Swashbuckle.AspNetCore.Annotations;

namespace CarpoolWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CarControllers : ControllerBase
    {
        private readonly ICarsService carsService;
        public CarControllers(ICarsService cars)
        {
            carsService = cars;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Get All Cars",
            Description = "Get all cars with the relevant to the user properties")]
        public async Task<IActionResult> GetCarsAsync()
        {
            try
            {
                var cars = await this.carsService.GetAllAsync();
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        // Get Car by Id or Registration?
        [HttpGet("{id}/car")]
        [SwaggerOperation(Summary = "Get Car by Id",
                    Description = "Get a specific Car by the Id property")]
        public async Task<IActionResult> GetCarByIdAsync(int id)
        {
            try
            {
                var cars = await this.carsService.GetByIdAsync(id);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpPost("")]
        [SwaggerOperation(Summary = "Create Car",
            Description = "Create a New Car")]
        public async Task<IActionResult> CreateCarAsync([BindRequired] string registration, [BindRequired] int userId, [BindRequired] int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed)
        {
            try
            {
                var cars = await this.carsService.CreateCarAsync(registration, userId, totalSeats, brand, model, color, canSmoke, petsAllowed);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        [HttpDelete("")]
        [SwaggerOperation(Summary = "Delete Car", 
            Description = "Delete a specific car by its Registration and OwnerId properties")]
        public async Task<IActionResult> DeleteOwnCarAsync([BindRequired] int userId, [BindRequired] string registration)
        {
            try
            {
                await this.carsService.DeteleOwnCarAsync(userId, registration);
                return this.Ok("Successfully deleted");
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
        
        [HttpPut("")]
        [SwaggerOperation(Summary = "Update Car",
            Description = "Update the properties of a specific Car")]
        public async Task<IActionResult> UpdateCarAsync([BindRequired] string registration, [BindRequired] int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed)
        {
            try
            {
                var cars = await this.carsService.UpdateAsync(registration, userId, totalSeats, brand, model, color, canSmoke, petsAllowed);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("/query")]
        [SwaggerOperation(Summary = "Filter Multiple Criteria",
            Description = "Filter all Cars by mutlitple criteria")]
        public async Task<IActionResult> FilterMultipleCriteriaAsync([FromQuery] bool petsAllowed, bool canSmoke)
        {
            try
            {
                var cars = await this.carsService.MultipleQueryFilterAsync(petsAllowed, canSmoke);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/owner")]
        [SwaggerOperation(Summary = "Get Cars by OwnerId",
            Description = "Get the collection of cars by their specific OwnerId property")]
        public async Task<IActionResult> GetByOwnerAsync([FromRoute] int id)
        {
            try
            {
                var cars = await this.carsService.GetByOwnerAsync(id);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("filter/smoke")]
        [SwaggerOperation(Summary = "Filter by Smoke",
            Description = "Filter Cars by the CanSmoke property")]
        public async Task<IActionResult> FilterBySmokeAsync([FromQuery] bool smoke)
        {
            try
            {
                var cars = await this.carsService.FilterBySmokeAsync(smoke);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("filter/pets")]
        [SwaggerOperation(Summary = "Filter by Pets",
            Description = "Filter Cars by the PetsAllowed property")]
        public async Task<IActionResult> FilterByPetsAsync([FromQuery] bool pets)
        {
            try
            {
                var cars = await this.carsService.FilterByPetsAllowerdAsync(pets);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}