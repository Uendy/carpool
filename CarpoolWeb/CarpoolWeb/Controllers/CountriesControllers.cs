﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

using CarpoolService.Contracts;
using CarpoolService.Exception;

namespace CarpoolWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountriesController : ControllerBase
    {
        private readonly ICountriesService countriesService;
        public CountriesController(ICountriesService countries)
        {
            this.countriesService = countries;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets All Countries", 
            Description = "Get all countries")]
        public async Task<IActionResult> GetCountries()
        {
            try
            {
                var countries = await this.countriesService.GetCountriesAsync();
                return this.Ok(countries);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Gets Country by Id",
            Description = "Get a specific country by Id parameter")]
        public async Task<IActionResult> GetCountriesId([FromRoute] int id)
        {
            try
            {
                var countries = await this.countriesService.GetCountriesByIdAsync(id);
                return this.Ok(countries);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }
    }
}