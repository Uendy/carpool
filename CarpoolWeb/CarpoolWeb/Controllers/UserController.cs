﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.AspNetCore.Http;

namespace CarpoolWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService user)
        {
            this.userService = user;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets all Users",
            Description = "Gets all the users and the useful information for the user.")]
        public async Task<IActionResult> GetUsersAsync()
        {
            try
            {
                var users = await this.userService.GetAll();
                return this.Ok(users);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpGet("/{id}")]
        [SwaggerOperation(Summary = "Gets User by Id",
            Description = "Gets a specific user by their unique Id")]
        public async Task<IActionResult> GetUserByIdAsync([FromRoute] int id)
        {
            try
            {
                var user = await this.userService.GetByIdAsync(id);
                return this.Ok(user);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpPut("")]
        [SwaggerOperation(Summary = "Updates User",
            Description = "Updates the properties of a specific user")]
        public async Task<IActionResult> UpdateUserAsync([BindRequired] int userId, string username, string password, string firstName, string lastName, string email, string phoneNumber)
        {
            try
            {
                var user = await this.userService.UpdateAsync(userId, username, password, firstName, lastName, email, phoneNumber);
                return this.Ok(user);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("")]
        [SwaggerOperation(Summary = "Creates User",
            Description = "Create a new user")]
        public async Task<IActionResult> CreateUserAsync([BindRequired] string userName, [BindRequired] string password, [BindRequired] string firstName, [BindRequired] string lastName, [BindRequired] string email, [BindRequired] string phoneNumber,IFormFile avatar)
        {
            try
            {
                string validationString = "sssss";
                var user = await this.userService.CreateAsync(userName, password, firstName, lastName, email, phoneNumber, validationString,avatar);
                return this.Ok(user);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("/seach/username/{username}")]
        [SwaggerOperation(Summary = "Search by Username",
            Description = "Searches for a specific user by username property")]
        public async Task<IActionResult> GetByUsernameAsync([FromRoute] string username)
        {
            try
            {
                var users = await this.userService.GetByUsernameAsync(username);
                return this.Ok(users);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("search/email/{email}")]
        [SwaggerOperation(Summary = "Searches by Email ",
            Description = "Searches for a specific user by email property")]
        public async Task<IActionResult> SearchByEmaileAsync([FromRoute] string email)
        {
            try
            {
                var users = await this.userService.SearchByEmailAsync(email);
                return this.Ok(users);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("search/phone/{phone}")]
        [SwaggerOperation(Summary = "Search by Phone",
            Description = "Searches for a specific user by phone property")]
        public async Task<IActionResult> GetByPhoneAsync([FromRoute] string phone)
        {
            try
            {
                var users = await this.userService.GetByPhoneAsync(phone);
                return this.Ok(users);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // Should this be: order users by rating?
        [HttpGet("filter/{rating}")]
        [SwaggerOperation(Summary = "Search by Rating",
            Description = "Searches for a specific user by rating property")]
        public async Task<IActionResult> FilterByRatingAsync([FromRoute] decimal rating)
        {
            try
            {
                var users = await this.userService.FilterByRatingAsync(rating);
                return this.Ok(users);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("search")]
        [SwaggerOperation(Summary = "Search Multiple Criteria",
            Description = "Search users by multiple criteria")]
        public async Task<IActionResult> FilterMultipleCriteriaAsync([FromQuery] string firstname, string lastname, string email, string phone, string username)
        {
            try
            {
                var users = await this.userService.MultipleQueryFilterAsync(firstname, lastname, email, phone, username);
                return this.Ok(users);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Delete User",
            Description = "Deletes a specific user")]
        public async Task<IActionResult> DeleteOwnUserAsync([BindRequired] int id)
        {
            try
            {
                await this.userService.DeleteOwnUserAsync(id);
                return this.Ok("Successfully deleted");
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpDelete("/review")]
        [SwaggerOperation(Summary = "Delete Review",
            Description = "Deletes a comment the user wrote")]
        public async Task<IActionResult> DeleteReviewAsync([BindRequired] int userId, [BindRequired] int reviewId)
        {
            try
            {
                await this.userService.DeleteReviewAsync(userId, reviewId);
                return this.Ok("Successfully deleted");
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/cars")]
        [SwaggerOperation(Summary = "Get User Cars",
            Description = "Gets the cars of a specific user")]
        public async Task<IActionResult> GetUserCarsAsync(int id)
        {
            try
            {
                var cars = await this.userService.SeeAllHisCarsAsync(id);
                return this.Ok(cars);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/recipient/review")]
        [SwaggerOperation(Summary = "Get User Recieved Reviews",
            Description = "Gets the recieved reviews of a specific user")]
        public async Task<IActionResult> GetUserRecipientReviewsAsync(int id)
        {
            try
            {
                var reviews = await this.userService.SeeAllHisRecipientReviewsAsync(id);
                return this.Ok(reviews);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/author/review")]
        [SwaggerOperation(Summary = "Get User Authored Reviews",
            Description = "Gets the authored reviews of a specific user")]
        public async Task<IActionResult> GetUserAuthorReviewsAsync(int id)
        {
            try
            {
                var reviews = await this.userService.SeeAllHisAuthorReviewsAsync(id);
                return this.Ok(reviews);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/rating")]
        [SwaggerOperation(Summary = "User Average Rating",
            Description = "Gets the average rating of a specific user")]
        public async Task<IActionResult> GetAverageRatingAsync(int id)
        {
            try
            {
                var rating = await this.userService.AverageRatingAsync(id);
                return this.Ok(rating);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost("/review")]
        [SwaggerOperation(Summary = "Create Review",
            Description = "Creates a review from a specific user to a specific user")]
        public async Task<IActionResult> CreateReviewAsync([BindRequired] int userId, [BindRequired] int recipientId, string comment, [BindRequired] decimal rating)
        {
            try
            {
                var review = await this.userService.LeaveReviewAsync(userId, recipientId, comment, rating);
                return this.Ok(review);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut("/review")]
        [SwaggerOperation(Summary = "Edit Review",
            Description = "Edit a review that a user wrote")]
        public async Task<IActionResult> EditReviewAsync([BindRequired] int userId, [BindRequired] int reviewId, string comment, [BindRequired] decimal rating)
        {
            try
            {
                var review = await this.userService.EditReviewAsync(userId, reviewId, comment, rating);
                return this.Ok(review);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}/owner/history")]
        [SwaggerOperation(Summary = "Get User Travel History",
            Description = "Gets the travevl history of a specific user")]
        public async Task<IActionResult> OwnerTravelHistoryAsync(int id)
        {
            try
            {
                var travels = await this.userService.OwnTravelHistory(id);
                return this.Ok(travels);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("/recipientRequests")]
        [SwaggerOperation(Summary = "Get Users Recipient Requests",
            Description = "Get all the Recipient Requests for a specific User by Id")]
        public async Task<IActionResult> GetAllRecipientRequest([BindRequired] int recipientId)
        {
            try
            {
                var requests = await this.userService.SeeAllHisRecipientRequestAsync(recipientId);
                return this.Ok(requests);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("/authorRequests")]
        [SwaggerOperation(Summary = "Get Users Author Requests",
            Description = "Get all the Author Requests for a specific User by Id")]
        public async Task<IActionResult> GetAllAuthorRequest([BindRequired] int authorId)
        {
            try
            {
                var requests = await this.userService.SeeAllHisAuthorRequestAsync(authorId);
                return this.Ok(requests);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost("/create/request")]
        [SwaggerOperation(Summary = "Create Travel Request",
            Description = "Create a new Travel Request to an existing Travel")]
        public async Task<IActionResult> CreateTravelRequest([BindRequired] int authorId, [BindRequired] int recipientId, [BindRequired] int travelId)
        {
            try
            {
                var request = await this.userService.CreateRequestAsync(authorId, recipientId, travelId);
                return this.Ok(request);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut("/respond/request")]
        [SwaggerOperation(Summary = "Respond to Travel Request",
            Description = "Respond to a specific Travel Request")]
        public async Task<IActionResult> RespondToTravelRequest([BindRequired] int recipientId, [BindRequired] int requestId, [BindRequired] bool answer)
        {
            try
            {
                var request = await this.userService.EditRequestAsync(recipientId, requestId, answer);
                return this.Ok(request);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpDelete("/requests")]
        [SwaggerOperation(Summary = "Delete Requests",
            Description = "Delete a Users authored Request")]
        public async Task<IActionResult> DeleteTravelRequest([BindRequired] int authorId, [BindRequired] int requestId)
        {
            try
            {
                await this.userService.DeleteRequestAsync(authorId, requestId);
                return this.Ok("Successfully deleted");
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}