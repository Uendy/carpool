﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

using CarpoolService.Contracts;
using CarpoolService.Exception;

namespace CarpoolWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReviewControllers : ControllerBase
    {
        private readonly IReviewService reviewService;
        public ReviewControllers(IReviewService review)
        {
            this.reviewService = review;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Get All Reviews",
            Description = "Get all Reviews")]
        public async Task<IActionResult> GetAllReviews()
        {
            try
            {
                var review = await this.reviewService.GetAllAsync();
                return this.Ok(review);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get Review by Id", 
            Description = "Get a specific Review by Id")]
        public async Task<IActionResult> GetReviewId([FromRoute] int id)
        {
            try
            {
                var review = await this.reviewService.GetByIdAsync(id);
                return this.Ok(review);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpPut("")]
        [SwaggerOperation(Summary = "Update Review",
            Description = "Update properties of a specific Review")]
        public async Task<IActionResult> UpdateReview([BindRequired] int id, string comment, decimal rating)
        {
            try
            {
                var rev = await this.reviewService.UpdateAsync(id, comment, rating);
                return this.Ok(rev);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}