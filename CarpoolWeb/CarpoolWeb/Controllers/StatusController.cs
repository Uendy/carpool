﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

using CarpoolService.Contracts;
using CarpoolService.Exception;

namespace CarpoolWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatusController : ControllerBase
    {
        private readonly IStatusService statusService;
        public StatusController(IStatusService status)
        {
            this.statusService = status;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Get All Statuses",
            Description = "Get All Statuses")]
        public async Task<IActionResult> GetAllStatuses()
        {
            try
            {
                var statuses = await this.statusService.GetAllAsync();
                return this.Ok(statuses);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);

            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get Status by Id", 
            Description = "Get a specific Status by Id property")]
        public async Task<IActionResult> GetStatusById([FromRoute] int id)
        {
            try
            {
                var review = await this.statusService.GetByIdAsync(id);
                return this.Ok(review);
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500, ex.Message);
            }
        }

        [HttpPut("")]
        [SwaggerOperation(Summary = "Update Status",
            Description = "Update the Status Type property of a specific status")]
        public async Task<IActionResult> UpdateStatus([BindRequired] int id, [BindRequired] string statusType)
        {
            try
            {
                var statues = await this.statusService.UpdateAsync(id, statusType);
                return this.Ok(statues);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}