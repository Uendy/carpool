using CarpoolData.CarpoolDBContext;
using CarpoolService.Contracts;
using CarpoolService.ModelService;
using CloudinaryDotNet;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace CarpoolWeb
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddControllersWithViews();
            services.AddDbContext<CarpoolDBContect>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers();
            services.AddScoped<IMailClient, MailClient>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<ICountriesService, CountriesService>();
            services.AddScoped<ICarsService, CarsService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IStatusService, StatusService>();
            services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IAdminsService, AdminsService>();
            services.AddScoped<ITravelsService, TravelService>();
            services.AddScoped<ICloudinaryService, CloudinaryService>();
            var cloudinary = new Cloudinary(new Account("dnnibpx64", "955872244291882", "JZ5B1JnCLt059vch_CUujbBaycI"));
            services.AddSingleton(cloudinary);


            services.AddSwaggerGen(c =>
            {
                c.EnableAnnotations();
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CarpoolWeb", Version = "v1" });
                var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";

                var filePath = Path.Combine(AppContext.BaseDirectory, fileName);
                //c.IncludeXmlComments(filePath);
            });

            // For the Annotations of Swagger methods
            //services.AddSwaggerGen(c => { c.EnableAnnotations(); });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CarpoolWeb v1"));
            }


            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
