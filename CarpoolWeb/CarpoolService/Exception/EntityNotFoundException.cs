﻿using System;

namespace CarpoolService.Exception
{
  public  class EntityNotFoundException: ApplicationException
    {
        public EntityNotFoundException()
        {

        }
        public EntityNotFoundException(string message)
            : base(message)
        {

        }
    }
}
