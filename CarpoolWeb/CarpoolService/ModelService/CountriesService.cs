﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class CountriesService : ICountriesService
    {
        private readonly CarpoolDBContect Db;
        public CountriesService(CarpoolDBContect db)
        {
            this.Db = db;
        }
        public async Task<IEnumerable<CountryDTO>> GetCountriesAsync()
        {
            var countries = await this.Db.Countries
                .Include(r => r.Cities)
                .Select(u => new CountryDTO(u))
                .ToListAsync();

            if (countries == null || countries.Count() == 0)
            { 
                throw new EntityNotFoundException("No countries exist");
            }

            return countries;

        }

        public async Task<CountryDTO> GetCountriesByIdAsync(int countryId)
        {
            var country = await this.Db.Countries
                         .Include(t => t.Cities)
                         .FirstOrDefaultAsync(x => x.Id == countryId);

            if (country == null)
            {
                throw new EntityNotFoundException($"Country with Id: {countryId} does not exist");
            }

            var response = new CountryDTO(country);
            return response;
        }
    }
}
