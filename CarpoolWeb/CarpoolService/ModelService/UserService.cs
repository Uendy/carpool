﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper;
using CarpoolData.DTOAndMapper.UserDTO;
using CarpoolData.Models;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class UserService : IUserService
    {
        private readonly CarpoolDBContect Db;
        private readonly ICloudinaryService cloudinary;
        public UserService(CarpoolDBContect db,ICloudinaryService cloudinary)
        {
            this.Db = db;
            this.cloudinary = cloudinary;
        }

        public async Task<decimal> AverageRatingAsync(int userId)
        {
            var reviews = await this.Db.Reviews
                .Where(x => x.RecipientId == userId)
                .ToListAsync();

            if (reviews.Count() == 0 || reviews == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any ratings");
            }

            var averageRating = Math.Round(reviews.Average(x => x.Rating), 2);

            return averageRating;
        }

        public async Task<UsersDTO> CreateAsync(string userName, string password, string firstName, string lastName, string email, string phoneNumber, string validationString,IFormFile avatar)
        {
            var user = new User(userName, password, firstName, lastName, email, phoneNumber, validationString);

            if (avatar!=null)
            {
                string fileName = avatar.FileName;

                user.ProfileImage = await this.cloudinary.UploadPictureAsync( avatar, fileName, userName);
            }
            else
            {
                user.ProfileImage = "https://res.cloudinary.com/dnnibpx64/image/upload/v1638988721/22-223968_default-profile-picture-circle-hd-png-download_excz9m.jpg";
            }
            this.Db.Users.Add(user);

            await this.Db.SaveChangesAsync();

            user = await this.Db.Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.UserName == userName);

            var response = new UsersDTO(user);
            return response;
        }
        //TODO
        public async Task DeleteReviewAsync(int userId, int reviewId)
        {
            var reviewToDelete = await this.Db.Reviews
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .FirstOrDefaultAsync(x => x.Id == reviewId
                && x.AuthorId == userId);

            if (reviewToDelete == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} has not written a review with Id: {reviewId}");
            }

            reviewToDelete.IsDelete = true;
            this.Db.Reviews.Remove(reviewToDelete);

            await this.Db.SaveChangesAsync();
        }
        //TODO
        public async Task DeleteOwnUserAsync(int userId)
        {
            var user = await this.Db.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            user.IsDelete = true;
            this.Db.Remove(user);

            await this.Db.SaveChangesAsync();
        }
        //TODO
        public async Task<ReviewDTO> EditReviewAsync(int userId, int reviewId, string comment, decimal rating)
        {
            var reviewToUpdate = await this.Db.Reviews
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .FirstOrDefaultAsync(x => x.Id == reviewId 
                && x.AuthorId == userId);

            if (reviewToUpdate == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} has not written a review with Id: {reviewId}");
            }

            if (comment != null)
            {
                reviewToUpdate.Comment = comment;
            }

            if (rating != default(decimal))
            {
                reviewToUpdate.Rating = rating;
            }

            await Db.SaveChangesAsync();
            return new ReviewDTO(reviewToUpdate);
        }

        public async Task<IEnumerable<UsersDTO>> FilterByRatingAsync(decimal rating)
        {
            var users = await this.Db.Users
                .Where(x => x.RecipientReviews.Average(x => x.Rating) == rating)
                .Select(x => new UsersDTO(x))
                .ToListAsync();
            //var users = await GetAll();

            //users = users.Where(x => x.Reviews.Average(x => x.Rating) == rating);

            if (users == null || users.Count() == 0)
            {
                throw new EntityNotFoundException($"Users with Rating: {rating} not found");
            }

            return users;
        }

        public async Task<IEnumerable<UsersDTO>> GetAll()
        {
            var users = await this.Db.Users
                .Include(r => r.Role)
                .Select(u => new UsersDTO(u))
                .ToListAsync();

            if (users.Count() == 0 || users == null)
            { 
                throw new EntityNotFoundException("No users exist");
            }

            return users;
        }

        public async Task<UsersDTO> GetByIdAsync(int id)
        {
            var user = await this.Db.Users
                        .Include(t => t.Travel)
                        .Include(x => x.Cars)
                        .Include(r => r.Role)
                        .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {id} does not exist");
            }

            var response = new UsersDTO(user); // this could throw an exception if null (we only check later)

            return response;
        }
        //TODO its bad practis too cast 
        public async Task<IEnumerable<UsersDTO>> GetByPhoneAsync(string phone)
        {
            var response = await MultipleQueryFilterAsync(null, null, null, phone, null);

            if (response == null || response.Count() == 0)
            {
                throw new EntityNotFoundException($"Users with Phone: {phone} not found");
            }

            return response;
        }

        public async Task<UsersDTO> GetByUsernameAsync(string username)
        {
            var user = await this.Db.Users
                .Include(t => t.Travel)
                .Include(x => x.Cars)
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.UserName.Equals(username)); // should this be contains?

            if (user == null)
            {
                throw new EntityNotFoundException($"User with Username: {username} does not exist");
            }

            var response = new UsersDTO(user);

            return response;
        }
        //TODO comment with alex for 2 collection for review 
        public async Task<ReviewDTO> LeaveReviewAsync(int userId, int recipientId, string comment, decimal rating)
        {
            var user = GetFullUserAsync(userId).Result;
            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            //user.AuthorReviews.Add(review);
            var recipient = GetFullUserAsync(recipientId).Result;
            if (recipient == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            var review = new Review(userId, recipientId, comment, rating);
            review.Author = user;
            review.Recipient = recipient;

            user.AuthorReviews.Add(review);
            recipient.RecipientReviews.Add(review);

            await this.Db.Reviews.AddAsync(review);

            await Db.SaveChangesAsync();

            return new ReviewDTO(review);
        }

        public async Task<IEnumerable<UsersDTO>> MultipleQueryFilterAsync(string firstName, string lastName, string email, string phone, string username)
        {
            // does this sort of method break the single responsibility?

            // Can I find out if this method is called in the call stack by another and can an if tell me where to throw the exception
            // Example: No users with email in email search or no users match query in this method
            var result = await GetAll();

            if (firstName != null)
            {
                result = result.Where(x => x.FirstName.Contains(firstName));
            }
            if (lastName != null)
            {
                result = result.Where(x => x.LastName.Contains(lastName));
            }
            if (email != null)
            {
                result = result.Where(x => x.Email.Contains(email));
            }
            if (phone != null)
            {
                result = result.Where(x => x.PhoneNumber.Contains(phone));
            }
            if (username != null)
            {
                result = result.Where(x => x.Username.Contains(username));
            }

            // Rating?

            return result;
        }
        //TODO
        public async Task<IEnumerable<TravelDTO>> OwnTravelHistory(int userId)
        {
            var user = await GetByIdAsync(userId);

            var travels = user.TravelHistory;

            var travelsAsDTO = travels.Select(x => new TravelDTO(x));

            return travelsAsDTO;
        }

        public async Task<IEnumerable<UsersDTO>> SearchByEmailAsync(string email)
        {
            var result = await MultipleQueryFilterAsync(null, null, email, null, null);

            if (result == null || result.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Email: {email} not found");
            }

            return result;
        }

        public async Task<IEnumerable<CarDTO>> SeeAllHisCarsAsync(int userId)
        {
            var user = await GetFullUserAsync(userId);

            var cars = user.Cars.ToList();

            if (cars == null || cars.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any cars");
            }

            var carsAsDTO = cars.Select(x => new CarDTO(x)).ToList();

            return carsAsDTO;
        }

        public async Task<IEnumerable<ReviewDTO>> SeeAllHisRecipientReviewsAsync(int userId)
        {
            var user = await GetFullUserAsync(userId);

            var reviews = await this.Db.Reviews
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .Where(x => x.RecipientId == userId)
                .ToListAsync();

            if (reviews == null || reviews.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any recipient reviews");
            }

            var reviewsAsDTO = reviews.Select(x => new ReviewDTO(x));

            return reviewsAsDTO;
        }

        public async Task<IEnumerable<ReviewDTO>> SeeAllHisAuthorReviewsAsync(int userId)
        {
            var user = await GetFullUserAsync(userId);

            var reviews = await this.Db.Reviews
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .Where(x => x.AuthorId == userId)
                .ToListAsync();

            if (reviews == null || reviews.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any author reviews");
            }

            var reviewsAsDTO = reviews.Select(x => new ReviewDTO(x)).ToList();

            return reviewsAsDTO;
        }

        public async Task<UsersDTO> UpdateAsync(int userId, string username, string password, string firstName, string lastName, string email, string phoneNumber)
        {
            var user = await this.Db.Users
                .Include(x => x.Role)
                //.Include(x => x.Travel)
                //.Include(x => x.Cars)
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            if (username != null)
            {
                if (await this.Db.Users.AnyAsync(x => x.UserName == username))
                {
                    throw new ArgumentException($"Username: {username} is already taken");
                }
                user.UserName = username;
            }

            if (password != null)
            {
                user.Password = password;
            }

            if (firstName != null)
            {
                user.FirstName = firstName;
            }

            if (lastName != null)
            {
                user.LastName = lastName;
            }

            if (email != null)
            {
                if (await this.Db.Users.AnyAsync(x => x.Email == email))
                {
                    throw new ArgumentException($"Email: {email} is already taken");
                }
                user.Email = email;
            }

            if (phoneNumber != null)
            {
                if (await this.Db.Users.AnyAsync(x => x.PhoneNumber == phoneNumber))
                {
                    throw new ArgumentException($"Phone Number: {phoneNumber} is already taken");
                }
                user.PhoneNumber = phoneNumber;
            }


            await Db.SaveChangesAsync();

            return new UsersDTO(user);
        }

        public async Task<IEnumerable<RequestDTO>> SeeAllHisRecipientRequestAsync(int userId)
        {
            var requests = await this.Db.Requests
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .Where(x => x.RecipientId == userId)
                .ToListAsync();

            if (requests == null || requests.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any recipient requests");
            }

            var requestAsDTO = requests.Select(x => new RequestDTO(x));

            return requestAsDTO;
        }

        public async Task<IEnumerable<RequestDTO>> SeeAllHisAuthorRequestAsync(int userId)
        {
            var requests = await this.Db.Requests
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .Where(x => x.AuthorId == userId)
                .ToListAsync();

            if (requests == null || requests.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any author requests");
            }

            var requestAsDTO = requests.Select(x => new RequestDTO(x));

            return requestAsDTO;
        }

        public async Task<RequestDTO> CreateRequestAsync(int userId, int recipientId, int travelId)
        {

            var user = GetFullUserAsync(userId).Result;
            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            var recipient = GetFullUserAsync(recipientId).Result;
            if (recipient == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            // User only has 1 current travel, travel history, but cant plan future travels until current is complete
            if (recipient.TravelId != travelId)
            {
                throw new EntityNotFoundException($"User with Id: {recipientId} does not have a Travel with Id: {travelId}");
            }

            var request = new Request(userId, recipientId, travelId);
            request.Author = user;
            request.Recipient = recipient;

            user.AuthorRequests.Add(request);
            recipient.RecipientRequests.Add(request);

            await this.Db.Requests.AddAsync(request);

            await Db.SaveChangesAsync();

            return new RequestDTO(request);
        }
        public async Task<RequestDTO> EditRequestAsync(int userId, int requestId, bool answer)
        {
            var requestToUpdate = await this.Db.Requests
            .Include(x => x.Author)
            .Include(x => x.Recipient)
            .FirstOrDefaultAsync(x => x.Id == requestId
            && x.AuthorId == userId);

            if (requestToUpdate == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} has not written a Request with Id: {requestId}");
            }

            if (answer == false)
            {
                requestToUpdate.Status = RequestEnum.Declined;
            }
            else
            {
                requestToUpdate.Status = RequestEnum.Accepted;
            }


            await Db.SaveChangesAsync();
            return new RequestDTO(requestToUpdate);
        }

        public async Task DeleteRequestAsync(int userId, int requestId)
        {
            var requestToDelete = await this.Db.Requests
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .FirstOrDefaultAsync(x => x.Id == requestId
                && x.AuthorId == userId);

            if (requestToDelete == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} has not written a Request with Id: {requestId}");
            }

            requestToDelete.IsDelete = true;

            await this.Db.SaveChangesAsync();
        }

        // Method without a controller, only used to help out other services
        public async Task<User> GetFullUserAsync(int userId)
        {
            var user = await this.Db.Users
                .Include(x => x.Cars)
                .Include(x => x.Role)
                //.Include(x => x.RecipientReviews) //??
                //.Include(x => x.AuthorReviews)
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
            }

            return user;
        }

        // TODO: Add user to travel only if it request == accepted

        //TODO, type byte[]
        //public async Task<UsersDTO> UploadPhotoAsync()
        //{

        //}
    }
}
