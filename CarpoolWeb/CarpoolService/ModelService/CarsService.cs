﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper;
using CarpoolData.Models;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class CarsService : ICarsService
    {
        private readonly CarpoolDBContect Db;
        private readonly IUserService userService;

        // is this DIP failure?

        public CarsService(CarpoolDBContect db, IUserService userService)
        {
            this.Db = db;
            this.userService = userService;
        }
        public async Task<CarDTO> CreateCarAsync(string registration, int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed)
        {
            var owner = await this.Db.Users
                .Include(x => x.Cars)
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (owner == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist");
            }

            var car = new Car(registration, userId, totalSeats, brand, model, color, canSmoke, petsAllowed, owner);

            // async AddAsync
            await this.Db.Cars.AddAsync(car);

            await this.Db.SaveChangesAsync();

            //car = await this.Db.Cars
            //    .Include(x => x.Owner)
            //    .FirstOrDefaultAsync(x => x.Registration == registration);

            var respons = new CarDTO(car);

            return respons;
        }

        public async Task DeteleOwnCarAsync(int userId, string registration)
        {
            //var owner = userService.GetFullUserAsync(userId).Result;

            var car = await this.Db.Cars
                .Where(c => c.UserId == userId)
                .FirstOrDefaultAsync(c => c.Registration == registration);

            if (car == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have car with Registration: {registration}");
            }

            car.IsDelete = true;
            this.Db.Cars.Remove(car);

            await this.Db.SaveChangesAsync();
        }

        public async Task<IEnumerable<CarDTO>> FilterByPetsAllowerdAsync(bool petsAllowed)
        {
            var cars = await this.Db.Cars
                .Include(x => x.User)
                .Include(x => x.Travel)
                .Where(x => x.PetsAllowed == petsAllowed)
                .ToListAsync();

            if (cars == null || cars.Count() == 0)
            {
                if (petsAllowed == true)
                {
                    throw new EntityNotFoundException($"Cars where you can bring pets are not found");
                }
                throw new EntityNotFoundException($"No cars found matching your query");
            }

            var carsAsDTO = cars.Select(x => new CarDTO(x));

            return carsAsDTO;
        }

        public async Task<IEnumerable<CarDTO>> FilterBySmokeAsync(bool canSmoke)
        {
            var cars = await this.Db.Cars
                .Include(x => x.User)
                .Include(x => x.Travel)
                .Where(x => x.CanSmoke == canSmoke)
                .ToListAsync();

            if (cars == null || cars.Count() == 0)
            {
                if (canSmoke == true)
                {
                    throw new EntityNotFoundException($"Cars where you can smoke are not found");
                }
                throw new EntityNotFoundException($"Cars where you can not smoke are not found");
            }

            var carsAsDTO = cars.Select(x => new CarDTO(x));

            return carsAsDTO;
        }

        public async Task<IEnumerable<CarDTO>> GetAllAsync()
        {
            var cars = await this.Db.Cars
                .Include(x => x.User)
                .Include(r => r.Travel)
                .Select(u => new CarDTO(u))
                .ToListAsync();

            if (cars == null || cars.Count() == 0)
            {
                throw new EntityNotFoundException("No cars exist");
            }

            return cars;
        }

        public async Task<CarDTO> GetByIdAsync(int id)
        {
            var car = await this.Db.Cars
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (car == null)
            {
                throw new EntityNotFoundException($"Car with Id: {id} does not exist");
            }

            var response = new CarDTO(car);

            return response;
        }

        public async Task<IEnumerable<CarDTO>> GetByOwnerAsync(int userId)
        {
            var cars = await this.Db.Cars
                 .Include(t => t.User)
                 .Where(x => x.UserId == userId)
                 .ToListAsync();

            if (cars == null || cars.Count() == 0)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have any cars");
            }

            var carsAsDTO = cars.Select(x => new CarDTO(x));

            return carsAsDTO;
        }

        public async Task<IEnumerable<CarDTO>> MultipleQueryFilterAsync(bool petsAllowed, bool canSmoke)
        {
            var cars = await this.Db.Cars
                .Include(x => x.User)
                .Include(r => r.Travel)
                .Where(x => x.PetsAllowed == petsAllowed && x.CanSmoke == canSmoke)
                .ToListAsync();

            if (cars == null || cars.Count() == 0)
            {
                throw new EntityNotFoundException("No cars match your query");
            }

            var result = cars.Select(x => new CarDTO(x));
            return result;
        }

        public async Task<CarDTO> UpdateAsync(string registration, int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed)
        {
            //var owner = userService.GetFullUserAsync(userId).Result;

            //if (!owner.Cars.Any(x => x.Registration == registration))
            //{
            //    throw new EntityNotFoundException($"User: {owner.UserName} does not have a car with Registration: {registration}");
            //}

            var carToUpdate = await this.Db.Cars
                .Include(x => x.Owner)
                .FirstOrDefaultAsync(u => u.Registration == registration && u.UserId == userId);

            if (carToUpdate == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not have a car with Registration: {registration}");
            }

            if (totalSeats != 0)
            {
                carToUpdate.TotalSeats = totalSeats;
            }

            if (brand != null)
            {
                carToUpdate.Brand = brand;
            }

            if (model != null)
            {
                carToUpdate.Model = model;
            }

            if (color != null)
            {
                carToUpdate.Color = color;
            }

            if (canSmoke == true)
            {
                carToUpdate.CanSmoke = true;
            }

            if (petsAllowed == true)
            {
                carToUpdate.PetsAllowed = true;
            }


            await Db.SaveChangesAsync();

            var response = new CarDTO(carToUpdate);

            return response;
        }

        // Helped method without controller:
        public async Task<Car> GetFullCarAsync(int carId)
        {
            var car = await this.Db.Cars
                .Include(x => x.Owner)
                .FirstOrDefaultAsync(x => x.Id == carId);

            if (car == null)
            {
                throw new EntityNotFoundException($"Car with Id: {carId} does not exist");
            }

            return car;
        }
    }
}