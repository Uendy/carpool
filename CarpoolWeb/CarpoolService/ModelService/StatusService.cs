﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper.UserDTO;
using CarpoolData.Models;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class StatusService : IStatusService
    {
        private readonly CarpoolDBContect Db;
        public StatusService(CarpoolDBContect db)
        {
            this.Db = db;
        }
        public async Task<IEnumerable<StatusDTO>> GetAllAsync()
        {
            var statuses = await this.Db.Statuses
                .Include(r => r.Travels)
                .Select(u => new StatusDTO(u))
                .ToListAsync();

            if (statuses == null || statuses.Count() == 0)
            { 
                throw new EntityNotFoundException("No statuses exist");
            }

            return statuses;
        }

        public async Task<StatusDTO> GetByIdAsync(int id)
        {
            var status = await this.Db.Statuses
                        .Include(t => t.Travels)
                        .FirstOrDefaultAsync(x => x.Id == id);

            if (status == null)
            {
                throw new EntityNotFoundException($"Status with Id: {id} does not exist");
            }

            var response = new StatusDTO(status);
            return response;
        }

        public async Task<StatusDTO> UpdateAsync(int id, string statusType)
        {
            // need to redo
            var status = await this.Db.Statuses
                .FirstOrDefaultAsync(u => u.Id == id);

            if(status == null)
            {
                throw new EntityNotFoundException($"Status with Id: {id} does not exist");
            }

            status.StatusType = statusType;

            await Db.SaveChangesAsync();

            return new StatusDTO(status);
        }
    }
}
