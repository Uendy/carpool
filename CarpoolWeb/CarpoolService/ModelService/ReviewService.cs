﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper;
using CarpoolData.Models;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class ReviewService : IReviewService
    {
        private readonly CarpoolDBContect Db;
        public ReviewService(CarpoolDBContect db)
        {
            this.Db = db;
        }
        public async Task<IEnumerable<ReviewDTO>> GetAllAsync()
        {
            var reviews = await this.Db.Reviews
                .Include(r => r.Author)
                .Include(r => r.Recipient)
                .Select(u => new ReviewDTO(u))
                .ToListAsync();

            if (reviews == null || reviews.Count() == 0)
            { 
                throw new EntityNotFoundException("No reviews exist");
            }

            return reviews;
        }

        public async Task<ReviewDTO> GetByIdAsync(int id)
        {
            var review = await this.Db.Reviews
                .Include(t => t.Author)
                .Include(r => r.Recipient)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (review == null)
            {
                throw new EntityNotFoundException($"Review with Id: {id} does not exist");
            }

            var response = new ReviewDTO(review); 

            return response;
        }

        public async Task<ReviewDTO> UpdateAsync(int id, string comment, decimal rating)
        {
            var review = await this.Db.Reviews
                .Include(x => x.Author)
                .Include(x => x.Recipient)
                .FirstOrDefaultAsync(u => u.Id == id);

            if (review == null)
            {
                throw new EntityNotFoundException($"Review with Id: {id} does not exist");
            }

            if (comment != null)
            {
                review.Comment = comment;
            }

            if (rating != null)
            {
                review.Rating = rating;
            }

            await Db.SaveChangesAsync();

            return new ReviewDTO(review);
        }
    }
}
