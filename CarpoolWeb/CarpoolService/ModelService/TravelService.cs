﻿using CarpoolData.CarpoolDBContext;
using CarpoolData.DTOAndMapper;
using CarpoolData.Models;
using CarpoolService.Contracts;
using CarpoolService.Exception;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class TravelService : ITravelsService
    {
        private readonly CarpoolDBContect Db;
        private readonly ICarsService carsService;
        public TravelService(CarpoolDBContect db, ICarsService carsService)
        {
            this.Db = db;
            this.carsService = carsService;
        }

        // Should the travel methods return all travels or only status that hasnt left?

        public async Task<TravelDTO> CreateTravelAsync(DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId)
        {
            var travel = new Travel(departureTime, arrivalTime, availableSeats, breaks, startLocationId, destinationLocationId, carId, statusId);
            travel.Car = carsService.GetFullCarAsync(carId).Result;

            // see if car has enough seats for available seats?

            await this.Db.Travels.AddAsync(travel);

            await this.Db.SaveChangesAsync();

            var id = travel.Id;

            travel = await this.Db.Travels
                .Include(x => x.StartLocation)
                .Include(x => x.Destination)
                .Include(x => x.Car)
                .Include(x => x.Status)
                .FirstOrDefaultAsync(x => x.Id == id);

            // Is there a problem with EF and the foreign key ref from travel to carId?? // Its different from the others

            var response = new TravelDTO(travel);

            return response;
        }

        public async Task<IEnumerable<TravelDTO>> GetAllAsync()
        {
            var travels = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                .Where(x => x.Car != null) // this is only temporary for the travels I made while trying to do the create travel method as car is null for them
                .ToListAsync();

            if (travels == null || travels.Count() == 0)
            {
                throw new EntityNotFoundException("There are currently no travels.");
            }

            var response = travels.Select(x => new TravelDTO(x));

            return response;
        }
        public async Task<TravelDTO> GetByIdAsync(int travelId)
        {
            var travel = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                .FirstOrDefaultAsync(x => x.Id == travelId);

            if (travel == null)
            {
                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
            }

            var response = new TravelDTO(travel);

            return response;
        }

        public async Task<IEnumerable<TravelDTO>> GetByDepartureTimeAsync(DateTime departureTime)
        {
            //2021-11-17T15:51:35Z this is the datetime format swagger uses, will need to either split the datetime in seperate parameters or split the letters out?

            var timePattern = "yyyy-MM-dd-HH-mm";
            var departureTimeDT = departureTime.ToString(timePattern);


            var travels = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                //.Where(x => x.DepartureTime
                //    .ToString(timePattern).Equals(departureTimeDT)) ? wont allow me to put it here?
                .ToListAsync();

            travels = travels.Where(x => x.DepartureTime.ToString(timePattern) == departureTimeDT).ToList();

            if (travels == null || travels.Count() == 0)
            {
                throw new EntityNotFoundException($"Travels with Departure Time: {departureTimeDT} do not exist.");
            }

            var response = travels.Select(x => new TravelDTO(x));

            return response;
        }

        // should it accept an exact address or just addressId and do I check it?
        public async Task<IEnumerable<TravelDTO>> GetByStartLocationAsync(int addressId)
        {
            var travels = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                .Where(x => x.StartLocation.Id == addressId)
                .ToListAsync();

            if (travels == null || travels.Count() == 0)
            {
                throw new EntityNotFoundException($"Travels with Start Location Id : {addressId} do not exist.");
            }

            var response = travels.Select(x => new TravelDTO(x));

            return response;
        }

        public async Task<IEnumerable<TravelDTO>> GetByDestinationAsync(int addressId)
        {
            var travels = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                .Where(x => x.Destination.Id == addressId)
                .ToListAsync();

            if (travels == null || travels.Count() == 0)
            {
                throw new EntityNotFoundException($"Travels with Destination Location Id : {addressId} do not exist.");
            }

            var response = travels.Select(x => new TravelDTO(x));

            return response;
        }

        public async Task<IEnumerable<TravelDTO>> GetByAvailableSeatsAsync()
        {
            // should it be cars with atleast 1 available seat or cars with exactly x available seats?
            var travels = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                .Where(x => x.AvailableSeat > 0)
                .ToListAsync();

            if (travels == null || travels.Count() == 0)
            {
                throw new EntityNotFoundException($"No Travels with available seats.");
            }

            var response = travels.Select(x => new TravelDTO(x));

            return response;
        }
        public async Task<IEnumerable<TravelDTO>> GetByStatusAsync(Status status)
        {
            var travels = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Where(x => x.Status.Id == status.Id)
                .ToListAsync();

            if (travels == null || travels.Count() == 0)
            {
                throw new EntityNotFoundException($"No Travels with Status: {status.StatusType} available.");
            }

            var response = travels.Select(x => new TravelDTO(x));

            return response;
        }
        //public async Task<IEnumerable<TravelDTO>> MultipleQueryFilterAsync();
        public async Task<TravelDTO> UpdateAsync(int travelId, DateTime departureTime, DateTime arrivalTime, int availableSeats, bool breaks, int startLocationId, int destinationLocationId, int carId, int statusId)
        {
            var travelToUpdate = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .FirstOrDefaultAsync(x => x.Id == travelId);

            if (travelToUpdate == null)
            {
                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
            }

            if (departureTime != default(DateTime))
            {
                travelToUpdate.DepartureTime = departureTime;
            }

            if (arrivalTime != default(DateTime))
            {
                travelToUpdate.ArrivalTime = arrivalTime;
            }

            if (availableSeats != default(decimal))
            {
                travelToUpdate.AvailableSeat = availableSeats;
            }

            if (breaks == true)
            {
                travelToUpdate.Breaks = true;
            }

            if (startLocationId != 0)
            {
                if (!this.Db.Addresses.Any(x => x.Id == startLocationId))
                {
                    throw new EntityNotFoundException($"Addess with Id: {startLocationId} does not exist");
                }
                travelToUpdate.StartLocationId = startLocationId;
            }

            if (destinationLocationId != 0)
            {
                if (!this.Db.Addresses.Any(x => x.Id == destinationLocationId))
                {
                    throw new EntityNotFoundException($"Addess with Id: {destinationLocationId} does not exist");
                }
                travelToUpdate.DestinationId = destinationLocationId;
            }

            if (carId != 0)
            {
                if (!this.Db.Cars.Any(x => x.Id == carId))
                {
                    throw new EntityNotFoundException($"Car with Id: {carId} does not exist");
                }
                travelToUpdate.CarId = carId;
            }

            if (statusId != 0)
            {
                if (!this.Db.Statuses.Any(x => x.Id == statusId))
                {
                    throw new EntityNotFoundException($"Status with Id: {statusId} does not exist");
                }
                travelToUpdate.StatusId = statusId;
            }

            await Db.SaveChangesAsync();

            var response = new TravelDTO(travelToUpdate);

            return response;
        }
        public async Task<TravelDTO> DeleteOwnTravelAsync(int travelId)
        {
            var travelToDelete = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .FirstOrDefaultAsync(x => x.Id == travelId);

            if (travelToDelete == null)
            {
                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
            }

            travelToDelete.IsDelete = true;
            this.Db.Travels.Remove(travelToDelete);

            await Db.SaveChangesAsync();

            var response = new TravelDTO(travelToDelete);

            return response;
        }
        public async Task<TravelDTO> AddUserToTravelAsync(int ownerId, int travelId, int userId)
        {
            var travel = await this.Db.Travels
                .Include(x => x.Car)
                .Include(x => x.Status)
                .Include(x => x.Destination)
                .Include(x => x.StartLocation)
                .Include(x => x.Passenger)
                .Where(x => x.Status.Id == 1 || x.StatusId == 2)
                .FirstOrDefaultAsync(x => x.Id == travelId);

            if (travel == null)
            {
                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
            }

            if (travel.Car.UserId != ownerId)
            {
                throw new EntityNotFoundException($"OwnerId: {ownerId} does not match the travel's ownerId");
            }


            if (travel.AvailableSeat == 0)
            {
                throw new ArgumentOutOfRangeException($"Travel with Id: {travelId} does not have any available seats.");
            }

            var user = await this.Db.Users
                .Include(x => x.Travel)
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {
                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
            }

            travel.Passenger.Add(user);
            travel.AvailableSeat--;
            user.Travel = travel;
            user.TravelHistory.Add(travel);

            var response = new TravelDTO(travel);

            return response;

        }
        //public async Task<IEnumerable<TravelDTO>> RequestToTravelAsync();
        //public async Task<IEnumerable<TravelDTO>> RespondToTravelAsync()
        //{if(resposne == true => AddUserToTravelAsync(travelId, userId))}
        //public async Task<TravelDTO> AddStopToTravelAsync();
    }
}
