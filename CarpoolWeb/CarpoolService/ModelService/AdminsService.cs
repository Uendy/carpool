﻿//using CarpoolData.CarpoolDBContext;
//using CarpoolData.DTOAndMapper;
//using CarpoolData.DTOAndMapper.UserDTO;
//using CarpoolData.Models;
//using CarpoolService.Contracts;
//using CarpoolService.Exception;
//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace CarpoolService.ModelService
//{
//    public class AdminsService : IAdminsService
//    {
//        private readonly CarpoolDBContect Db;
//        public AdminsService(CarpoolDBContect db)
//        {
//            this.Db = db;
//        }

//        // Admin Services For User:
//        public async Task<UsersDTO> GetUserByIdAsync(int userId)
//        {
//            var user = await this.Db.Users
//                            .Include(t => t.Travel)
//                            .Include(x => x.Cars)
//                            .Include(r => r.Role)
//                            .FirstOrDefaultAsync(x => x.Id == userId);
//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }
//            var userAsDTO = new UsersDTO(user);

//            return userAsDTO;
//        }

//        public async Task<IEnumerable<UsersDTO>> GetAllUsers()
//        {
//            var users = await this.Db.Users
//                            .Include(t => t.Travel)
//                            .Include(x => x.Cars)
//                            .Include(r => r.Role)
//                            .ToListAsync();

//            if (users == null)
//            {
//                throw new EntityNotFoundException("There are no users.");
//            }

//            var usersAsDTO = users.Select(x => new UsersDTO(x));

//            return usersAsDTO;
//        }

//        public async Task<UsersDTO> GetUserByUsernameAsync(string username)
//        {
//            var user = await this.Db.Users
//                            .Include(t => t.Travel)
//                            .Include(x => x.Cars)
//                            .Include(r => r.Role)
//                            .FirstOrDefaultAsync(x => x.UserName == username);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"There are no users with Name: {username}.");
//            }

//            var userAsDTO = new UsersDTO(user);

//            return userAsDTO;
//        }

//        public async Task<UsersDTO> CreateUserAsync(User user)
//        {
//            this.Db.Users.Add(user);
//            await this.Db.SaveChangesAsync();

//            var userAsDTO = new UsersDTO(user);

//            return userAsDTO;
//        }

//        public async Task<IEnumerable<UsersDTO>> SearchUserByEmailAsync(string email)
//        {
//            return await MultipleQueryFilterUsersAsync(null, null, email, null, null);
//        }

//        public async Task<UsersDTO> GetUserByPhoneAsync(string phone)
//        {
//            var user = await this.Db.Users
//                            .Include(t => t.Travel)
//                            .Include(x => x.Cars)
//                            .Include(r => r.Role)
//                            .FirstOrDefaultAsync(x => x.PhoneNumber == phone);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Phone: {phone} does not exist");
//            }

//            var userAsDTO = new UsersDTO(user);

//            return userAsDTO;
//        }

//        public async Task<IEnumerable<UsersDTO>> FilterUsersByRatingAsync(decimal rating)
//        {
//            var users = await GetAllUsers();

//            return users.Where(x => x.AverageRating == rating);
//        }

//        public async Task<IEnumerable<UsersDTO>> MultipleQueryFilterUsersAsync(string firstName, string lastName, string email, string phone, string username)
//        {
//            var result = this.Db.Users.Include(r => r.Role).Select(u => new UsersDTO(u));

//            if (firstName != null)
//            {
//                result = result.Where(x => x.FirstName.Contains(firstName));
//            }
//            if (lastName != null)
//            {
//                result = result.Where(x => x.LastName.Contains(lastName));
//            }
//            if (email != null)
//            {
//                result = result.Where(x => x.Email.Contains(email));
//            }
//            if (phone != null)
//            {
//                result = result.Where(x => x.PhoneNumber.Contains(phone));
//            }
//            if (email != null)
//            {
//                result = result.Where(x => x.Username.Contains(username));
//            }

//            return await result.ToArrayAsync();
//        }

//        public async Task<UsersDTO> UpdateUserAsync(int userId, User user)
//        {
//            var oldUser = await this.Db.Users.FirstOrDefaultAsync(x => x.Id == userId);
//            oldUser = user;

//            await Db.SaveChangesAsync();

//            return new UsersDTO(oldUser);
//        }

//        public async Task DeleteUserAsync(int userId)
//        {
//            var user = await GetUserByIdAsync(userId);

//            // this might not use the soft-delete
//            this.Db.Remove(user);

//            await Db.SaveChangesAsync();
//        }

//        public async Task<IEnumerable<CarDTO>> GetAllUsersCarsAsync(int userId)
//        {
//            var user = await this.Db.Users
//                            .Include(t => t.Travel)
//                            .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            var cars = user.Cars;

//            if (cars == null || cars.Count() == 0)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not have any cars.");
//            }

//            var carsAsDTO = cars.Select(x => new CarDTO(x));

//            return carsAsDTO;
//        }

//        public async Task<IEnumerable<ReviewDTO>> GetUsersReviewsAsync(int userId)
//        {
//            var user = await this.Db.Users
//                            .Include(r => r.AuthorReviews)
//                            .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            var reviews = user.AuthorReviews;

//            if (reviews == null || reviews.Count() == 0)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not have any reviews.");
//            }

//            var reviewsAsDTO = reviews.Select(x => new ReviewDTO(x));

//            return reviewsAsDTO;
//        }

//        public async Task<decimal> GetUserAverageRatingAsync(int userId)
//        {
//            var user = await this.Db.Users
//                            .Include(r => r.AuthorReviews)
//                            .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            if (user.AuthorReviews.Count() == 0)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not have any ratings.");
//            }

//            var reviewAverage = user.AuthorReviews.Average(x => x.Rating);

//            return reviewAverage;
//        }

//        //Task<ReviewDTO> LeaveReviewAsync(int userId, int reciepentId, Review review); Leave comment for a user?
//        public async Task<ReviewDTO> EditUsersReviewAsync(int userId, int reviewId, Review review)
//        {
//            var user = await this.Db.Users
//                            .Include(r => r.AuthorReviews)
//                            .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            var reviewToUpdate = user.AuthorReviews.FirstOrDefault(x => x.Id == reviewId);

//            if (reviewToUpdate == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId}, does not have a review with id: {reviewId}");
//            }

//            reviewToUpdate = review;

//            await Db.SaveChangesAsync();

//            var reviewAsDTO = new ReviewDTO(reviewToUpdate);

//            return reviewAsDTO;
//        }

//        public async Task DeleteUsersCommentAsync(int userId, int reviewId)
//        {
//            var user = await this.Db.Users
//                            .Include(r => r.AuthorReviews)
//                            .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            var review = user.AuthorReviews.FirstOrDefault(x => x.Id == reviewId);

//            if (review == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId}, does not have a review with id: {reviewId}");
//            }

//            var commentToDelete = review.Comment;

//            // cant do it async?
//            this.Db.Remove(commentToDelete);

//            await this.Db.SaveChangesAsync();
//        }
//        //Task<UsersDTO> UploadPhotoAsync(); Update a users photo?

//        // Admin services for Travels:
//        public async Task<IEnumerable<TravelDTO>> GetUsersTravelHistory(int userId)
//        {
//            var user = await this.Db.Users
//                            .Include(t => t.Travel)
//                            .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            var travels = user.TravelHistory;

//            var travelsAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelsAsDTO;
//        }

//        // Admin Services For Cars:
//        public async Task<CarDTO> CreateCarAsync(Car car)
//        {
//            this.Db.Cars.Add(car);
//            await this.Db.SaveChangesAsync();

//            var carAsDTO = new CarDTO(car);

//            return carAsDTO;
//        }

//        public async Task<IEnumerable<CarDTO>> GetAllCarsAsync()
//        {
//            var cars = await this.Db.Cars
//                .Include(x => x.Travel)
//                .Include(x => x.User)
//                .ToListAsync();

//            if (cars == null)
//            {
//                throw new EntityNotFoundException($"There are no existing cars.");
//            }

//            var carsAsDTO = cars.Select(x => new CarDTO(x));

//            return carsAsDTO;
//        }

//        public async Task<CarDTO> GetCarByIdAsync(int carId)
//        {
//            var car = await this.Db.Cars
//                .Include(x => x.Travel)
//                .Include(x => x.User)
//                .FirstOrDefaultAsync(x => x.Id == carId);

//            if (car == null)
//            {
//                throw new EntityNotFoundException($"Car with Id: {carId} does not exist.");
//            }

//            var carAsDTO = new CarDTO(car);

//            return carAsDTO;
//        }

//        public async Task<IEnumerable<CarDTO>> GetCarsByOwnerAsync(int userId)
//        {
//            var cars = await this.Db.Cars
//               .Include(x => x.User)
//               .Where(x => x.Owner.Id == userId)
//               .ToListAsync();

//            if (cars == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not own any cars.");
//            }

//            var carsAsDTO = cars.Select(x => new CarDTO(x));

//            return carsAsDTO;
//        }

//        public async Task<IEnumerable<CarDTO>> FilterCarsBySmokeAsync(bool smoke)
//        {
//            var cars = await this.Db.Cars
//                .Include(x => x.User)
//                .Include(x => x.Travel)
//                //.Include(x => x.CanSmoke)
//                .Where(x => x.CanSmoke == smoke)
//                .ToListAsync();

//            if (cars == null)
//            {
//                if (smoke == true)
//                {
//                    throw new EntityNotFoundException($"Cars where you can smoke are not found.");
//                }
//                throw new EntityNotFoundException($"Cars where you can not smoke are not found.");
//            }

//            var carsAsDTO = cars.Select(x => new CarDTO(x));

//            return carsAsDTO;
//        }

//        public async Task<IEnumerable<CarDTO>> FilterCarsByPetsAllowerdAsync(bool pets)
//        {
//            var cars = await this.Db.Cars
//               .Include(x => x.User)
//               .Include(x => x.Travel)
//               //.Include(x => x.CanSmoke)
//               .Where(x => x.PetsAllowed == pets)
//               .ToListAsync();

//            if (cars == null)
//            {
//                if (pets == true)
//                {
//                    throw new EntityNotFoundException($"Cars where you can bring a pet are not found.");
//                }
//                // Not sure if you should be forced to bring a pet :D 
//                throw new EntityNotFoundException($"No cars found.");
//            }

//            var carsAsDTO = cars.Select(x => new CarDTO(x));

//            return carsAsDTO;
//        }
//        //Task<IEnumerable<CarDTO>> FilterByAirConditionerAsync();


//        public async Task<IEnumerable<CarDTO>> MultipleQueryFilterCarsAsync(bool PetsAllowed, bool CanSmoke)
//        {
//            var result = this.Db.Cars.Include(r => r.Travel).Select(u => new CarDTO(u));
//            result = result.Where(x => x.PetsAllowed == PetsAllowed);
//            result = result.Where(x => x.CanSmoke == CanSmoke);


//            //if (PetsAllowed != false)
//            //{
//            //    result = result.Where(x => x.PetsAllowed == PetsAllowed);
//            //}
//            //if (CanSmoke != false)
//            //{
//            //    result = result.Where(x => x.CanSmoke == CanSmoke);
//            //}
//            return await result.ToArrayAsync();
//        }


//        public async Task<CarDTO> UpdateCarAsync(int carId, Car car)
//        {
//            // cant use getCarByIdAsync as it returns carDTO != Car car
//            var carToUpdate = await this.Db.Cars
//                .FirstOrDefaultAsync(x => x.Id == carId);

//            if (carToUpdate == null)
//            {
//                throw new EntityNotFoundException($"Car with Id: {carId} does not exist");
//            }

//            carToUpdate = car;

//            await Db.SaveChangesAsync();

//            var carAsDTO = new CarDTO(carToUpdate);

//            return carAsDTO;
//        }
//        public async Task<CarDTO> DeteleCarAsync(int carId)
//        {
//            var carToDelete = await this.Db.Cars
//                .FirstOrDefaultAsync(x => x.Id == carId);

//            if (carToDelete == null)
//            {
//                throw new EntityNotFoundException($"Car with Id: {carId} does not exist");
//            }

//            carToDelete.IsDelete = true;

//            await Db.SaveChangesAsync();

//            var carAsDTO = new CarDTO(carToDelete);

//            return carAsDTO;
//        }

//        // Admin Services For Travels:
//        public async Task<TravelDTO> CreateTravelAsync(Travel travel)
//        {
//            this.Db.Travels.Add(travel);
//            await this.Db.SaveChangesAsync();

//            var travelAsDTO = new TravelDTO(travel);

//            return travelAsDTO;
//        }

//        public async Task<IEnumerable<TravelDTO>> GetAllTravelsAsync()
//        {
//            var travels = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .ToListAsync();

//            if (travels == null)
//            {
//                throw new EntityNotFoundException($"There are no existing travles.");
//            }

//            var travelsAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelsAsDTO;
//        }

//        public async Task<TravelDTO> GetTravelByIdAsync(int travelId)
//        {
//            var travel = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .FirstOrDefaultAsync(x => x.Id == travelId);

//            if (travel == null)
//            {
//                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist");
//            }

//            var travelAsDTO = new TravelDTO(travel);

//            return travelAsDTO;
//        }

//        // maybe not the minute aswell? Just day and hour?
//        public async Task<IEnumerable<TravelDTO>> GetTravelsByDepartureTimeAsync(DateTime departureTime)
//        {
//            var travels = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .Where(x => x.DepartureTime == departureTime)
//                .ToListAsync();

//            if (travels == null)
//            {
//                throw new EntityNotFoundException($"Travels with Departure Time: {departureTime.ToString("dd-MM-yyyy-HH-mm-ss")} do not exist");
//            }

//            var travelAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelAsDTO;
//        }

//        // Same as previous method:

//        public async Task<IEnumerable<TravelDTO>> GetTravelsByStartLocationAsync(DateTime arrivalTime)
//        {
//            var travels = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .Where(x => x.ArrivalTime == arrivalTime)
//                .ToListAsync();

//            if (travels == null)
//            {
//                throw new EntityNotFoundException($"Travels with Arrival Time: {arrivalTime.ToString("dd-MM-yyyy-HH-mm-ss")} do not exist");
//            }

//            var travelAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelAsDTO;
//        }

//        public async Task<IEnumerable<TravelDTO>> GetTravelsByDestinationAsync(Address destination)
//        {
//            var travels = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .Where(x => x.Destination == destination)
//                .ToListAsync();

//            if (travels == null)
//            {
//                throw new EntityNotFoundException($"Travels with Destination: {destination.Name} do not exist.");
//            }

//            var travelAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelAsDTO;
//        }

//        // whats our logic here? with an exact amount of avaliable seats, of it has atleast one?
//        public async Task<IEnumerable<TravelDTO>> GetTravelsByAvailableSeatsAsync(int availableSeats)
//        {
//            var travels = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .Where(x => x.AvailableSeat == availableSeats)
//                .ToListAsync();

//            if (travels == null)
//            {
//                throw new EntityNotFoundException($"Travels with {availableSeats} Available seats do not exist.");
//            }

//            var travelAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelAsDTO;
//        }

//        public async Task<IEnumerable<TravelDTO>> StatusTravelAsync(Status status)
//        {
//            var travels = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .Where(x => x.Status.StatusType == status.StatusType)
//                .ToListAsync();

//            if (travels == null)
//            {
//                throw new EntityNotFoundException($"Travels with Status Type: {status.StatusType} do not exist.");
//            }

//            var travelAsDTO = travels.Select(x => new TravelDTO(x));

//            return travelAsDTO;
//        }

//        public async Task<IEnumerable<TravelDTO>> MultipleQueryFilterTravelAsync()
//        {
//            // Neeed to see how it works: 
//            throw new NotImplementedException();
//        }

//        public async Task<TravelDTO> UpdateTravelAsync(int travelId, Travel travel)
//        {
//            var travelToUpdate = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .FirstOrDefaultAsync(x => x.Id == travelId);

//            if (travelToUpdate == null)
//            {
//                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
//            }

//            travelToUpdate = travel;

//            await Db.SaveChangesAsync();

//            var travelAsDTO = new TravelDTO(travelToUpdate);

//            return travelAsDTO;
//        }

//        public async Task<TravelDTO> DeleteTravelAsync(int travelId)
//        {
//            var travel = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .FirstOrDefaultAsync(x => x.Id == travelId);

//            if (travel == null)
//            {
//                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
//            }

//            travel.IsDelete = true;

//            await Db.SaveChangesAsync();

//            var travelAsDTO = new TravelDTO(travel);

//            return travelAsDTO;
//        }

//        public async Task<TravelDTO> AddUserToTravelAsync(int travelId, int userId)
//        {
//            var travel = await this.Db.Travels
//                .Include(x => x.Car)
//                .Include(x => x.Status)
//                .Include(x => x.Destination)
//                .Include(x => x.StartLocation)
//                .FirstOrDefaultAsync(x => x.Id == travelId);

//            if (travel == null)
//            {
//                throw new EntityNotFoundException($"Travel with Id: {travelId} does not exist.");
//            }

//            var user = await this.Db.Users
//                .FirstOrDefaultAsync(x => x.Id == userId);

//            if (user == null)
//            {
//                throw new EntityNotFoundException($"User with Id: {userId} does not exist.");
//            }

//            travel.Passenger.Add(user);

//            await Db.SaveChangesAsync();

//            var travelAsDTO = new TravelDTO(travel);

//            return travelAsDTO;
//        }
//        //Task<IEnumerable<TravelDTO>> RequestToTravelAsync();
//        //Task<IEnumerable<TravelDTO>> RespondToTravelAsync();
//        //Task<TravelDTO> AddStopToTravelAsync();

//        // Others: // Address, City, Country, Role, Status

//        // Status 
//        public async Task<IEnumerable<StatusDTO>> GetAllStatusAsync()
//        {
//            var statuses = await this.Db.Statuses
//                .Include(x => x.Travels)
//                .ToListAsync();

//            if (statuses == null || statuses.Count() == 0)
//            {
//                throw new EntityNotFoundException("No statuses exist.");
//            }

//            var statusesAsDTO = statuses.Select(x => new StatusDTO(x));

//            return statusesAsDTO;
//        }

//        public async Task<StatusDTO> GetStatusByIdAsync(int id)
//        {
//            var status = await this.Db.Statuses
//                .Include(x => x.Travels)
//                .FirstOrDefaultAsync(x => x.Id == id);

//            if (status == null)
//            {
//                throw new EntityNotFoundException($"No status with Id: {id} exists.");
//            }

//            var statusAsDTO = new StatusDTO(status);

//            return statusAsDTO;
//        }

//        public async Task<StatusDTO> UpdateStatusAsync(int id, Status status)
//        {
//            var statusToUpdate = await this.Db.Statuses
//                   .Include(x => x.Travels)
//                   .FirstOrDefaultAsync(x => x.Id == id);

//            if (status == null)
//            {
//                throw new EntityNotFoundException($"No status with Id: {id} exists.");
//            }

//            statusToUpdate = status;

//            await this.Db.SaveChangesAsync();

//            var statusAsDTO = new StatusDTO(status);

//            return statusAsDTO;
//        }

//        // Review:
//        public async Task<IEnumerable<ReviewDTO>> GetAllReviewsAsync()
//        {
//            var reviews = await this.Db.Reviews
//                .Include(x => x.Author)
//                .ToListAsync();

//            if (reviews == null || reviews.Count() == 0)
//            {
//                throw new EntityNotFoundException("No reviews exist.");
//            }

//            var reviewsAsDTO = reviews.Select(x => new ReviewDTO(x));

//            return reviewsAsDTO;
//        }

//        public async Task<ReviewDTO> GetReviewByIdAsync(int id)
//        {
//            var review = await this.Db.Reviews
//                .Include(x => x.Recipient)
//                .FirstOrDefaultAsync(x => x.Id == id);

//            if (review == null)
//            {
//                throw new EntityNotFoundException($"No review with Id: {id} exists.");
//            }

//            var reviewAsDTO = new ReviewDTO(review);

//            return reviewAsDTO;
//        }

//        public async Task<ReviewDTO> UpdateReviewAsync(int id, Review review)
//        {
//            var reviewToUpdate = await this.Db.Reviews
//                   .Include(x => x.Author)
//                   .FirstOrDefaultAsync(x => x.Id == id);

//            if (reviewToUpdate == null)
//            {
//                throw new EntityNotFoundException($"No review with Id: {id} exists.");
//            }

//            reviewToUpdate = review;

//            await this.Db.SaveChangesAsync();

//            var reviewAsDTO = new ReviewDTO(review);

//            return reviewAsDTO;
//        }

//        // City
//        public async Task<IEnumerable<CityDTO>> GetAllCitiesAsync()
//        {
//            var cities = await this.Db.Cities
//                .Include(x => x.Country)
//                .Include(x => x.Addresses)
//                .ToListAsync();

//            if (cities == null || cities.Count() == 0)
//            {
//                throw new EntityNotFoundException($"No cities exist.");
//            }

//            var citiesAsDTO = cities.Select(x => new CityDTO(x));

//            return citiesAsDTO;
//        }

//        public async Task<CityDTO> GetCityByIdAsync(int cityId)
//        {
//            var city = await this.Db.Cities
//                .Include(x => x.Country)
//                .Include(x => x.Addresses)
//                .FirstOrDefaultAsync(x => x.Id == cityId);

//            if (city == null)
//            {
//                throw new EntityNotFoundException($"City with Id: {cityId} does not exist.");
//            }

//            var cityAsDTO = new CityDTO(city);

//            return cityAsDTO;
//        }

//        // Country

//        public async Task<IEnumerable<CountryDTO>> GetAllCountriesAsync()
//        {
//            var countries = await this.Db.Countries
//                .Include(x => x.Cities)
//                .ToListAsync();

//            if (countries == null || countries.Count() == 0)
//            {
//                throw new EntityNotFoundException($"No countries exist.");
//            }

//            var countriesAsDTO = countries.Select(x => new CountryDTO(x));

//            return countriesAsDTO;
//        }

//        public async Task<CountryDTO> GetCountryByIdAsync(int countryId)
//        {
//            var country = await this.Db.Countries
//                .Include(x => x.Cities)
//                .FirstOrDefaultAsync(x => x.Id == countryId);

//            if (country == null)
//            {
//                throw new EntityNotFoundException($"Country with Id: {countryId} does not exist.");
//            }

//            var countryAsDTO = new CountryDTO(country);

//            return countryAsDTO;
//        }
//    }
//}