﻿using CarpoolData.Models;
using CarpoolService.Contracts;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace CarpoolService.ModelService
{
    public class MailClient: IMailClient
    {

        public async Task SendValidationEmail(User user)
        {
            var apiKey = "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";

            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("carpool@abv.bg", "carpool");
            var subject = $"Carpool {user.UserName} validation !";
            var to = new EmailAddress($"{user.Email}", $"{user.UserName}");
            var plainTextContent = "";
            var htmlContent = $"<strong>http://localhost:5000/Auth/Login</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);

        }
    }
}