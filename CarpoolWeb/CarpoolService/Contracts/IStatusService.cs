﻿
using CarpoolData.DTOAndMapper.UserDTO;
using CarpoolData.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
  public  interface IStatusService
    {
        Task<IEnumerable<StatusDTO>> GetAllAsync();
        Task<StatusDTO> GetByIdAsync(int id);
        Task<StatusDTO> UpdateAsync(int id, string statusType);
    }
}
