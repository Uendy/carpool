﻿using CarpoolData.DTOAndMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
   public interface ICityService
    {
        Task<IEnumerable<CityDTO>> GetCitiesAsync();
        Task<CityDTO> GetCityByIdAsync(int cityId);
    }
}
