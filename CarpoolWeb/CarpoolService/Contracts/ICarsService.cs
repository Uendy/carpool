﻿using CarpoolData.DTOAndMapper;
using CarpoolData.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
   public interface ICarsService
    {

        Task<CarDTO> CreateCarAsync(string registration, int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed);
        Task<IEnumerable<CarDTO>> GetAllAsync();
        Task<CarDTO> GetByIdAsync(int id);
        Task<IEnumerable<CarDTO>> GetByOwnerAsync(int id);
        Task<IEnumerable<CarDTO>> FilterBySmokeAsync(bool CanSmoke);
        Task<IEnumerable<CarDTO>> FilterByPetsAllowerdAsync(bool PetsAllowed);
        Task<IEnumerable<CarDTO>> MultipleQueryFilterAsync(bool PetsAllowed, bool CanSmoke);
        Task<CarDTO> UpdateAsync(string registration, int userId, int totalSeats, string brand, string model, string color, bool canSmoke, bool petsAllowed);
        Task DeteleOwnCarAsync(int userId, string registration);

        // Method without constructor
        Task<Car> GetFullCarAsync(int carId);
    }
}
