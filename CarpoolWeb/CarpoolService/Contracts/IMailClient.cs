﻿using CarpoolData.Models;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
    public interface IMailClient
    {
       Task SendValidationEmail(User user);
    }
}
