﻿using CarpoolData.DTOAndMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
    public interface ICountriesService
    {
        Task<IEnumerable<CountryDTO>> GetCountriesAsync();
        Task<CountryDTO> GetCountriesByIdAsync(int countryId);
    }
}
