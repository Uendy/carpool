﻿//using CarpoolData.DTOAndMapper;
//using CarpoolData.DTOAndMapper.UserDTO;
//using CarpoolData.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace CarpoolService.Contracts
//{
//    public interface IAdminsService
//    {
//        // Should we break it up into smaller classes of services?
//        // Admin Services For User:

//        Task<UsersDTO> GetUserByIdAsync(int id);
//        Task<IEnumerable<UsersDTO>> GetAllUsers();
//        Task<UsersDTO> GetUserByUsernameAsync(string username);
//        Task<UsersDTO> CreateUserAsync(User user);
//        Task<IEnumerable<UsersDTO>> SearchUserByEmailAsync(string email);
//        Task<UsersDTO> GetUserByPhoneAsync(string phone);
//        Task<IEnumerable<UsersDTO>> FilterUsersByRatingAsync(decimal rating);
//        Task<IEnumerable<UsersDTO>> MultipleQueryFilterUsersAsync(string firstName, string lastName, string email,string phone, string username);
//        Task<UsersDTO> UpdateUserAsync(int id, User user);
//        Task DeleteUserAsync(int userId);
//        Task<IEnumerable<CarDTO>> GetAllUsersCarsAsync(int userId);
//        Task<IEnumerable<ReviewDTO>> GetUsersReviewsAsync(int userId);
//        Task<decimal> GetUserAverageRatingAsync(int userId);
//        //Task<ReviewDTO> LeaveReviewAsync(int userId, int reciepentId, Review review); Leave comment for a user?
//        Task<ReviewDTO> EditUsersReviewAsync(int userId, int reviewId, Review review);
//        Task DeleteUsersCommentAsync(int userId, int reviewId);
//        //Task<UsersDTO> UploadPhotoAsync(); Update a users photo?
//        Task<IEnumerable<TravelDTO>> GetUsersTravelHistory(int userId);

//        // Admin Services For Cars:
//        Task<CarDTO> CreateCarAsync(Car car);
//        Task<IEnumerable<CarDTO>> GetAllCarsAsync();
//        Task<CarDTO> GetCarByIdAsync(int carId);
//        Task<IEnumerable<CarDTO>> GetCarsByOwnerAsync(int userId);
//        Task<IEnumerable<CarDTO>> FilterCarsBySmokeAsync(bool smoke);
//        Task<IEnumerable<CarDTO>> FilterCarsByPetsAllowerdAsync(bool pets);
//        //Task<IEnumerable<CarDTO>> FilterByAirConditionerAsync();
//        Task<IEnumerable<CarDTO>> MultipleQueryFilterCarsAsync(bool PetsAllowed, bool CanSmoke);
//        Task<CarDTO> UpdateCarAsync(int id, Car car);
//        Task<CarDTO> DeteleCarAsync(int carId);

//        // Admin Services For Travels:
//        Task<TravelDTO> CreateTravelAsync(Travel travel);
//        Task<IEnumerable<TravelDTO>> GetAllTravelsAsync();
//        Task<TravelDTO> GetTravelByIdAsync(int travelId);
//        Task<IEnumerable<TravelDTO>> GetTravelsByDepartureTimeAsync(DateTime departureTime);
//        Task<IEnumerable<TravelDTO>> GetTravelsByStartLocationAsync(DateTime arrivalTime);
//        Task<IEnumerable<TravelDTO>> GetTravelsByDestinationAsync(Address destination);
//        Task<IEnumerable<TravelDTO>> GetTravelsByAvailableSeatsAsync(int availableSears);
//        Task<IEnumerable<TravelDTO>> StatusTravelAsync(Status status);
//        Task<IEnumerable<TravelDTO>> MultipleQueryFilterTravelAsync();
//        Task<TravelDTO> UpdateTravelAsync(int travelId, Travel travel);
//        Task<TravelDTO> DeleteTravelAsync(int travelId);
//        Task<TravelDTO> AddUserToTravelAsync(int travelId, int userId);
//        //Task<IEnumerable<TravelDTO>> RequestToTravelAsync();
//        //Task<IEnumerable<TravelDTO>> RespondToTravelAsync();
//        //Task<TravelDTO> AddStopToTravelAsync();

//        // Status 
//        Task<IEnumerable<StatusDTO>> GetAllStatusAsync();

//        Task<StatusDTO> GetStatusByIdAsync(int id);

//        Task<StatusDTO> UpdateStatusAsync(int id, Status status);

//        // Review 
//        Task<IEnumerable<ReviewDTO>> GetAllReviewsAsync();

//        Task<ReviewDTO> GetReviewByIdAsync(int id);

//        Task<ReviewDTO> UpdateReviewAsync(int id, Review review);

//        // City
//        Task<IEnumerable<CityDTO>> GetAllCitiesAsync();

//        Task<CityDTO> GetCityByIdAsync(int cityId);

//        // Country 
//        Task<IEnumerable<CountryDTO>> GetAllCountriesAsync();

//        Task<CountryDTO> GetCountryByIdAsync(int countryId);
//    }
//}
