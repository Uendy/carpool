﻿using CarpoolData.DTOAndMapper;
using CarpoolData.DTOAndMapper.UserDTO;
using CarpoolData.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CarpoolService.Contracts
{
    public interface  IUserService
    {
        Task<UsersDTO> GetByIdAsync(int id);
        Task<IEnumerable <UsersDTO>> GetAll();
        Task<UsersDTO> GetByUsernameAsync(string username);
        Task<UsersDTO> CreateAsync(string userName, string password, string firstName, string lastName, string email, string phoneNumber, string validationString, IFormFile avatar);
        Task <IEnumerable<UsersDTO>> SearchByEmailAsync(string email);
        Task<IEnumerable<UsersDTO>> GetByPhoneAsync(string phone);
        Task<IEnumerable<UsersDTO>> FilterByRatingAsync(decimal rating);
        Task<IEnumerable<UsersDTO>> MultipleQueryFilterAsync(string firstName, string lastName, string email, string phone, string username);
        Task<UsersDTO> UpdateAsync(int userId, string username, string password, string firstName, string lastName, string email, string phoneNumber);
        Task DeleteOwnUserAsync(int userId);
        Task<IEnumerable<CarDTO>> SeeAllHisCarsAsync(int userId);
        Task<IEnumerable<ReviewDTO>> SeeAllHisRecipientReviewsAsync(int userId);
        Task<IEnumerable<ReviewDTO>> SeeAllHisAuthorReviewsAsync(int userId);
        Task<decimal> AverageRatingAsync(int userId);
        Task<ReviewDTO> LeaveReviewAsync(int userId, int recipientId, string comment, decimal rating);
        Task<ReviewDTO> EditReviewAsync(int userId, int reviewId, string comment, decimal rating);
        Task DeleteReviewAsync(int userId, int reviewId);

        // Request Methods:
        Task<IEnumerable<RequestDTO>> SeeAllHisRecipientRequestAsync(int userId);
        Task<IEnumerable<RequestDTO>> SeeAllHisAuthorRequestAsync(int userId);
        Task<RequestDTO> CreateRequestAsync(int userId, int recipientId, int travelId);
        Task<RequestDTO> EditRequestAsync(int userId, int requestId, bool answer);
        Task DeleteRequestAsync(int userId, int requestId);

       // Task<UsersDTO> UploadPhotoAsync();
        Task<IEnumerable<TravelDTO>> OwnTravelHistory(int userId);
        //Task<IEnumerable<>>

        // Nota method with a controller, but just a helped method for other services to find the user
        Task<User> GetFullUserAsync(int userId);
    }
}