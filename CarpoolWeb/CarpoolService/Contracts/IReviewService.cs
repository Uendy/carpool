﻿using CarpoolData.DTOAndMapper;
using CarpoolData.DTOAndMapper.UserDTO;
using CarpoolData.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolService.Contracts
{
    public interface IReviewService
    {
        Task<IEnumerable<ReviewDTO>> GetAllAsync();
        Task<ReviewDTO> GetByIdAsync(int id);
        Task<ReviewDTO> UpdateAsync(int id, string comment, decimal rating);
    }
}
